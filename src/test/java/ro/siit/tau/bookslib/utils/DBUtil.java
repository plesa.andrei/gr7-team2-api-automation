package ro.siit.tau.bookslib.utils;

import ro.siit.tau.bookslib.models.AuthorModel;
import ro.siit.tau.bookslib.models.BookModel;
import ro.siit.tau.bookslib.models.GenreModel;
import ro.siit.tau.bookslib.models.UserModel;
import ro.siit.tau.bookslib.services.SelectDB;
import ro.siit.tau.bookslib.services.Table;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static ro.siit.tau.bookslib.services.BaseAPI.PARTIAL_PATH_FOR_DB;
import static ro.siit.tau.bookslib.tests.BaseTest.DATABASE_PASSWORD;
import static ro.siit.tau.bookslib.tests.BaseTest.DATABASE_USER;

public class DBUtil {
    private static Connection conn;
    private static PreparedStatement preparedStatement;

    private static void disconnect() {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void closeStatement() {
        if (preparedStatement != null) {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void closeAllAndDisconnect() {
        closeStatement();
        disconnect();
    }

    public static PreparedStatement getPreparedStatement(String query) throws SQLException {
        connect();
        preparedStatement = conn.prepareStatement(query);
        return preparedStatement;
    }

    private static void connect() throws SQLException {
        String DB_HOST = "jdbc:mysql"+PARTIAL_PATH_FOR_DB+"/bookslib?useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String DB_USER = DATABASE_USER;
        String DB_PASSWORD = DATABASE_PASSWORD;
        conn = DriverManager.getConnection(DB_HOST, DB_USER, DB_PASSWORD);
    }

    public static int getLastIDFromQuery(String myQuery) {
        try {
            PreparedStatement statement = DBUtil.getPreparedStatement(myQuery);
            ResultSet rs = statement.executeQuery();
            if (rs.last()) {
                return rs.getInt("ID");
            }

        } catch (SQLException e) {
            e.printStackTrace();


        }
        return -1;
    }

    public static int getDBCount(String myQuery) {
        int count = -1;

        try {
            PreparedStatement statement = DBUtil.getPreparedStatement(myQuery);
            ResultSet rs = statement.executeQuery();

            count = 0;
            while (rs.next()) {

                count++;
            }


        } catch (SQLException e) {
            e.printStackTrace();


        }

        closeAllAndDisconnect();
        return count;
    }

    public static int getDBCount(Table table) {

        String myQuery="";


        int count = -1;

        switch (table) {
            case AUTHOR:
                myQuery = "select * from "+Table.AUTHOR.toString().toLowerCase();
                break;
            case BOOK:
                myQuery = "select * from "+Table.BOOK.toString().toLowerCase();
                break;
            case GENRE:
                myQuery = "select * from "+Table.GENRE.toString().toLowerCase();
                break;
            case USER:
                myQuery = "select * from "+Table.USER.toString().toLowerCase();
                break;
        }

        try {
            PreparedStatement statement = DBUtil.getPreparedStatement(myQuery);
            ResultSet rs = statement.executeQuery();

            count = 0;
            while (rs.next()) {

                count++;
            }


        } catch (SQLException e) {
            e.printStackTrace();


        }

        closeAllAndDisconnect();
        return count;
    }

    public static int getNextAutoIncrement(Table table) {

        String queryString = "";

        int increment = -1;


        switch (table) {
            case AUTHOR:
                queryString = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE table_name='author'";
                break;
            case BOOK:
                queryString = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE table_name='book'";
                break;
            case GENRE:
                queryString = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE table_name='genre'";
                break;
            case USER:
                queryString = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE table_name='user' and table_schema='bookslib'";
                break;

        }

        try {

            PreparedStatement statement = DBUtil.getPreparedStatement(queryString);
            ResultSet rs = statement.executeQuery();

            rs.next();
            increment = rs.getInt("auto_increment");

            // System.out.println(rs.getInt("auto_increment"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        closeAllAndDisconnect();

        return increment;
    }

    public static boolean validID(Table table,int id){

        String queryString = "";

       boolean isPresent=false;


        switch (table) {
            case AUTHOR:
                queryString = "SELECT * FROM author WHERE id="+id;
                break;
            case BOOK:
                queryString = "SELECT * FROM book WHERE id="+id;
                break;
            case GENRE:
                queryString = "SELECT * FROM genre WHERE id="+id;
                break;
            case USER:
                queryString = "SELECT * FROM user WHERE id="+id;
                break;
        }

        try {

            PreparedStatement statement = DBUtil.getPreparedStatement(queryString);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){isPresent=true;}

            // System.out.println(rs.getInt("auto_increment"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        closeAllAndDisconnect();

        return isPresent;



    }

    public static List<Object> getListDataModelsFromDB(ResultSet rs, Table table) throws SQLException {

        List<Object> models=new ArrayList<>();


        switch (table) {
            case AUTHOR:
                while (rs.next()) {
                    AuthorModel authorModel = new AuthorModel();

                    authorModel.setId(rs.getInt("id"));
                    authorModel.setFirstName(rs.getString("fname"));
                    authorModel.setLastName(rs.getString("lname"));

                    models.add(authorModel);
                }
            case BOOK:
                while (rs.next()) {
                    AuthorModel authorModel=AuthorModel.class.cast(SelectDB.select(Table.AUTHOR, rs.getInt("author_id")).get(0));
                    authorModel.setId(rs.getInt("author_id"));


                    GenreModel genreModel=GenreModel.class.cast(SelectDB.select(Table.GENRE, rs.getInt("genre_id")).get(0));
                    genreModel.setId(rs.getInt("genre_id"));

                    BookModel bookModel = new BookModel();

                    bookModel.setId(rs.getInt("id"));
                    bookModel.setName(rs.getString("name"));
                    bookModel.setPublicationDate(rs.getString("publication_date"));
                    bookModel.setAuthor(authorModel);
                    bookModel.setGenre(genreModel);

                    models.add(bookModel);
                }
            case GENRE:
                while (rs.next()) {
                    GenreModel genreModel = new GenreModel();

                    genreModel.setId(rs.getInt("id"));
                    genreModel.setName(rs.getString("name"));

                    models.add(genreModel);
                }
            case USER:
                while (rs.next()) {
                    UserModel userModel = new UserModel();

                    userModel.setId(rs.getInt("id"));
                    userModel.setUsername(rs.getString("username"));
                    userModel.setPassword(rs.getString("password"));

                    models.add(userModel);
                }

        }

        closeAllAndDisconnect();

        return models;

    }
}
