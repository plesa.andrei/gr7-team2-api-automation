package ro.siit.tau.bookslib.services;

import ro.siit.tau.bookslib.utils.DBUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class SelectDB {

    public static List<Object> select(Table table)   {

        String queryString = "";
        List<Object> models=new ArrayList<>();

        switch (table) {
            case AUTHOR:
                queryString = "SELECT *  FROM author" ;
                break;
            case BOOK:
                queryString = "SELECT * FROM book";
                break;
            case GENRE:
                queryString = "SELECT * FROM genre";
                break;
            case USER:
                queryString = "SELECT * FROM user";
                break;

        }

        try {
            PreparedStatement statement = DBUtil.getPreparedStatement(queryString);
            ResultSet rs = statement.executeQuery();

            switch (table) {
                case AUTHOR:
                    return DBUtil.getListDataModelsFromDB(rs,Table.AUTHOR);
                case BOOK:
                    return DBUtil.getListDataModelsFromDB(rs,Table.BOOK);
                case GENRE:
                    return DBUtil.getListDataModelsFromDB(rs,Table.GENRE);
                case USER:
                    return DBUtil.getListDataModelsFromDB(rs,Table.USER);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return models;
    }

    public static List<Object> select(Table table, int id)   {

        String queryString = "";
        List<Object> models=new ArrayList<>();

        switch (table) {
            case AUTHOR:
                queryString = "SELECT *  FROM author WHERE id=" + id;
                break;
            case BOOK:
                queryString = "SELECT * FROM book WHERE id=" + id;
                break;
            case GENRE:
                queryString = "SELECT * FROM genre WHERE id=" + id;
                break;
            case USER:
                queryString = "SELECT * FROM user WHERE id=" + id;
                break;
        }

        try {
            PreparedStatement statement = DBUtil.getPreparedStatement(queryString);
            ResultSet rs = statement.executeQuery();

            switch (table) {
                case AUTHOR:
                    return DBUtil.getListDataModelsFromDB(rs,Table.AUTHOR);
                case BOOK:
                    return DBUtil.getListDataModelsFromDB(rs,Table.BOOK);
                case GENRE:
                    return DBUtil.getListDataModelsFromDB(rs,Table.GENRE);
                case USER:
                    return DBUtil.getListDataModelsFromDB(rs,Table.USER);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return models;
    }

}
