package ro.siit.tau.bookslib.services;


import ro.siit.tau.bookslib.utils.DBUtil;

import java.sql.PreparedStatement;

import java.sql.SQLException;

import static ro.siit.tau.bookslib.utils.DBUtil.closeAllAndDisconnect;

public class    DeleteDB {


    public static void delete(Table table, int id)   {

        String queryString = "";

        switch (table) {
            case AUTHOR:
                queryString = "DELETE  FROM author WHERE id=" + id;
                break;
            case BOOK:
                queryString = "DELETE  FROM book WHERE id=" + id;
                break;
            case GENRE:
                queryString = "DELETE  FROM genre WHERE id=" + id;
                break;
            case USER:
                queryString = "DELETE  FROM user WHERE id=" + id;
                break;
        }
        try {
            PreparedStatement statement = DBUtil.getPreparedStatement(queryString);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        closeAllAndDisconnect();

    }
}
