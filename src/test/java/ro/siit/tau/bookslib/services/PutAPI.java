package ro.siit.tau.bookslib.services;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import ro.siit.tau.bookslib.models.AuthorModel;
import ro.siit.tau.bookslib.models.BookModel;
import ro.siit.tau.bookslib.models.GenreModel;

import java.io.IOException;

import static ro.siit.tau.bookslib.tests.BaseTest.*;

public class PutAPI extends BaseAPI {

    public static String put(AuthorModel authorModel, int id) throws IOException {

        String uriText = PARTIAL_PATH+"/author/update/" + id + "";

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPut httpPut = new HttpPut(uriText);

        authenticationWithToken(httpPut);


//        String firstNameString = "{\"firstName\":\"" + authorModel.getFirstName() + "\",";
//        String lastNameString = "\"lastName\":\"" + authorModel.getLastName() + "\"}";

        httpPut.setEntity(new StringEntity(authorModelMapper(authorModel), ContentType.APPLICATION_JSON));

        HttpResponse httpResponse = httpClient.execute(httpPut);

//        showResponseInformation(httpResponse);

        return  getResponseString(httpResponse);

    }

    public static String put(BookModel bookModel, int id) throws IOException {

        String uriText = PARTIAL_PATH+"/book/update/" + id + "";

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPut httpPut = new HttpPut(uriText);

        authenticationWithToken(httpPut);


//        String firstNameString = "{\"author\":{\"id\":" + bookModel.getAuthor().getId() + ",\"firstName\":\"" + bookModel.getAuthor().getFirstName() + "\",";
//        String lastNameString = "\"lastName\":\"" + bookModel.getAuthor().getFirstName() + "\"},";
//
//
//        String genreNameString = "\"genre\": {\"id\":" + bookModel.getGenre().getId() + ",\"name\":\"" + bookModel.getGenre().getName() + "\"},";
//
//        String bookNameString = "\"id\":" + bookModel.getId() + ",\"name\":\"" + bookModel.getName() + "\",";
//        String publicationDateString = "\"publicationDate\":\"" + bookModel.getPublicationDate() + "\"}";


        httpPut.setEntity(new StringEntity(bookModelMapper(bookModel), ContentType.APPLICATION_JSON));

        HttpResponse httpResponse = httpClient.execute(httpPut);

//        showResponseInformation(httpResponse);

        return  getResponseString(httpResponse);


    }

    public static String put(GenreModel genreModel, int id) throws IOException {

        String uriText = PARTIAL_PATH+"/genre/update/" + id + "";

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpPut httpPut = new HttpPut(uriText);

        authenticationWithToken(httpPut);

        httpPut.setEntity(new StringEntity(genreModelMapper(genreModel), ContentType.APPLICATION_JSON));

        HttpResponse httpResponse = httpClient.execute(httpPut);

        //showResponseInformation(httpResponse);

        return  getResponseString(httpResponse);


    }
}
