package ro.siit.tau.bookslib.services;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.HttpClientBuilder;
import ro.siit.tau.bookslib.models.AuthorModel;
import ro.siit.tau.bookslib.models.BookModel;
import ro.siit.tau.bookslib.models.GenreModel;

import java.io.IOException;

public class DeleteAPI extends BaseAPI {

    public static String delete(Table table, int id) throws IOException {

        String uriText = "";

        switch (table) {
            case AUTHOR:
                uriText = PARTIAL_PATH+"/author/delete/" + id + "";
                break;
            case BOOK:
                uriText = PARTIAL_PATH+"/book/delete/" + id + "";
                break;
            case GENRE:
                uriText = PARTIAL_PATH+"/genre/delete/" + id + "";
                break;
        }

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpDelete httpDelete = new HttpDelete(uriText);

        authenticationWithToken(httpDelete);

        HttpResponse httpResponse = httpClient.execute(httpDelete);

        //showResponseInformation(httpResponse);


        return  getResponseString(httpResponse);


    }

    public static void delete(AuthorModel authorModel, int id) throws IOException {

        String uriText = PARTIAL_PATH+"/author/delete/" + id + "";

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpDelete httpDelete = new HttpDelete(uriText);

        authenticationWithToken(httpDelete);

        HttpResponse httpResponse = httpClient.execute(httpDelete);

        showResponseInformation(httpResponse);

    }

    public static void delete(BookModel bookModel, int id) throws IOException {

        String uriText = PARTIAL_PATH+"/book/delete/" + id + "";

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpDelete httpDelete = new HttpDelete(uriText);

        authenticationWithToken(httpDelete);

        HttpResponse httpResponse = httpClient.execute(httpDelete);

        showResponseInformation(httpResponse);

    }

    public static void delete(GenreModel genreModel, int id) throws IOException {

        String uriText = PARTIAL_PATH+"/genre/delete/" + id + "";

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpDelete httpDelete = new HttpDelete(uriText);

        authenticationWithToken(httpDelete);

        HttpResponse httpResponse = httpClient.execute(httpDelete);

        showResponseInformation(httpResponse);

    }


}
