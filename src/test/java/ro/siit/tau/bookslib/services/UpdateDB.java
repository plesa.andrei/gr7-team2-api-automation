package ro.siit.tau.bookslib.services;

import org.testng.annotations.Test;
import ro.siit.tau.bookslib.models.AuthorModel;
import ro.siit.tau.bookslib.models.BookModel;
import ro.siit.tau.bookslib.models.GenreModel;
import ro.siit.tau.bookslib.utils.DBUtil;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static ro.siit.tau.bookslib.utils.DBUtil.closeAllAndDisconnect;

public class UpdateDB {

    public static void update(AuthorModel authorModel,int id) {

        String firstNameString="",commaString=",",lastNameString="";

        if(authorModel.getFirstName()!=null){firstNameString="fname='"+authorModel.getFirstName()+"'";}

        if((authorModel.getFirstName()==null)||(authorModel.getLastName()==null)){commaString="";}

        if(authorModel.getLastName()!=null){lastNameString="lname='"+authorModel.getLastName()+"'";}

        String queryString = "UPDATE author SET "+firstNameString+commaString+lastNameString+" WHERE id="+id;

        try {
            PreparedStatement statement = DBUtil.getPreparedStatement(queryString);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        closeAllAndDisconnect();

        //System.out.println(queryString);
    }

    public static void update(BookModel bookModel,int id) {

        String nameString="",
               publicationDateString="",
               authorIDString="",
               genreIDString="";

        if(bookModel.getName()!=null){nameString="name='"+bookModel.getName()+"'";}

        if(bookModel.getPublicationDate()!=null){publicationDateString="publication_date='"+bookModel.getPublicationDate()+"'";}

        if(bookModel.getAuthor().getId()!=0){authorIDString="author_id='"+bookModel.getAuthor().getId()+"'";}

        if(bookModel.getGenre().getId()!=0){genreIDString="genre_id='"+bookModel.getGenre().getId()+"'";}

        List<String> stringList=new ArrayList<>();

        if(bookModel.getName()!=null){stringList.add(nameString);}
        if(bookModel.getPublicationDate()!=null){stringList.add(publicationDateString);}
        if(bookModel.getAuthor().getId()!=0){stringList.add(authorIDString);}
        if(bookModel.getGenre().getId()!=0){stringList.add(genreIDString);}

        String fieldString="";

        for (String str:stringList) {
            fieldString+=str+",";

        }

        if(!fieldString.equals("")){fieldString=fieldString.substring(0,fieldString.lastIndexOf(","));}


        String queryString = "UPDATE book SET "+fieldString+" WHERE id="+id;

        try {
            PreparedStatement statement = DBUtil.getPreparedStatement(queryString);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        closeAllAndDisconnect();

        // System.out.println(queryString);
    }

    public static void update(GenreModel genreModel,int id) {

        String queryString = "UPDATE genre SET name='"+genreModel.getName()+"' WHERE id="+id;

        try {
            PreparedStatement statement = DBUtil.getPreparedStatement(queryString);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        closeAllAndDisconnect();

        // System.out.println(queryString);
    }

//    @Test
//    public void test(){
//        BookModel bookModel=new BookModel();
//        AuthorModel authorModel=new AuthorModel();
//        GenreModel genreModel=new GenreModel();
//
////        authorModel.setFirstName("UP Author first name111");
////        authorModel.setLastName("UP Author last name111");
////        genreModel.setName("Relativity physics");
//
//       // bookModel.setId(2);
//
//        bookModel.setName("UP Book 102");//
//        bookModel.setPublicationDate("2002-01-01");//
//      authorModel.setId(3);//
//      genreModel.setId(2);//
//
//
//
//        bookModel.setAuthor(authorModel);
//        bookModel.setGenre(genreModel);
//
//
//
//        update(bookModel,444);
//    }
}
