package ro.siit.tau.bookslib.services;

import org.apache.commons.io.IOUtils;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;

import ro.siit.tau.bookslib.models.UserModel;
import ro.siit.tau.bookslib.tests.BaseTest;

import java.io.IOException;


public class BaseAPI {

    public static final String PARTIAL_PATH= BaseTest.baseURL;
    public static final String PARTIAL_PATH_FOR_DB= BaseTest.dataBaseURL;


    private static final String USERNAME="username123456789";
    private static final String PASSWORD="password1234";


    public static void addUser() throws IOException {

        String uriText = PARTIAL_PATH+"/user/add";

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpPost httpPost = new HttpPost(uriText);

       // authenticationWithToken(httpPost);

        httpPost.setEntity(new StringEntity("{\"username\":\""+USERNAME+"\",\"password\":\""+PASSWORD+"\"}", ContentType.APPLICATION_JSON));

        HttpResponse httpResponse = httpClient.execute(httpPost);

        //showResponseInformation(httpResponse);

    }

    public static String addUser(UserModel userModel) throws IOException {

        String uriText = PARTIAL_PATH+"/user/add";

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpPost httpPost = new HttpPost(uriText);

         authenticationWithToken(httpPost);

        httpPost.setEntity(new StringEntity("{\"username\":\""+userModel.getUsername()+"\",\"password\":\""+userModel.getPassword()+"\"}", ContentType.APPLICATION_JSON));

        HttpResponse httpResponse = httpClient.execute(httpPost);

        //showResponseInformation(httpResponse);

        return  getResponseString(httpResponse);


    }



    public static String getAuthToken() throws IOException {

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(PARTIAL_PATH+"/login");

        httpPost.setEntity(new StringEntity("{\"username\":\""+USERNAME+"\",\"password\":\""+PASSWORD+"\"}"));

        HttpResponse httpResponse = httpClient.execute(httpPost);
        Header authHeader = httpResponse.getFirstHeader("Authorization");

        System.out.println("token is:" + authHeader.getValue());

        return authHeader.getValue();

    }

    public static void authenticationWithToken(HttpGet httpget) throws IOException {

        String token = BaseTest.TOKEN;
        BasicHeader header = new BasicHeader("Authorization", token);
        httpget.addHeader(header);
    }

    public static void authenticationWithToken(HttpDelete httpDelete) throws IOException {

        String token = BaseTest.TOKEN;
        BasicHeader header = new BasicHeader("Authorization", token);
        httpDelete.addHeader(header);
    }

    public static void authenticationWithToken(HttpPost httpPost) throws IOException {

        String token = BaseTest.TOKEN;
        BasicHeader header = new BasicHeader("Authorization", token);
        httpPost.addHeader(header);
    }

    public static void authenticationWithToken(HttpPut httpPut) throws IOException {

        String token = BaseTest.TOKEN;
        BasicHeader header = new BasicHeader("Authorization", token);
        httpPut.addHeader(header);
    }

    public static void showResponseInformation(HttpResponse httpResponse) throws IOException {

        System.out.println(httpResponse.getStatusLine().getStatusCode());
        System.out.println(httpResponse.getStatusLine().getReasonPhrase());

        String responseBody = IOUtils.toString(httpResponse.getEntity().getContent());
        System.out.println(responseBody);
    }

    public static String getResponseString(HttpResponse httpResponse) throws IOException {

        return  IOUtils.toString(httpResponse.getEntity().getContent());
    }






}
