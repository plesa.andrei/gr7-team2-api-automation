package ro.siit.tau.bookslib.services;

import ro.siit.tau.bookslib.models.AuthorModel;
import ro.siit.tau.bookslib.models.BookModel;
import ro.siit.tau.bookslib.models.GenreModel;
import ro.siit.tau.bookslib.models.UserModel;
import ro.siit.tau.bookslib.utils.DBUtil;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static ro.siit.tau.bookslib.utils.DBUtil.closeAllAndDisconnect;

public class InsertDB {

    public static void insert(AuthorModel authorModel) {

        String queryString = "INSERT INTO author (fname,lname) VALUES ('" + authorModel.getFirstName() + "','"+authorModel.getLastName()+"')";

        try {
            PreparedStatement statement = DBUtil.getPreparedStatement(queryString);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        closeAllAndDisconnect();

        //System.out.println(queryString);
    }

    public static void insert(BookModel bookModel) {

        String queryString = "INSERT INTO book (name,publication_date,author_id,genre_id) VALUES" +
                " ('" + bookModel.getName() + "','"+bookModel.getPublicationDate()+"','"+bookModel.getAuthor().getId()+"','"+bookModel.getGenre().getId()+"')";

        try {
            PreparedStatement statement = DBUtil.getPreparedStatement(queryString);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        closeAllAndDisconnect();

        //System.out.println(queryString);
    }

    public static void insert(GenreModel genreModel) {

        String queryString = "INSERT INTO genre (name) VALUES ('" + genreModel.getName() + "')";

        try {
            PreparedStatement statement = DBUtil.getPreparedStatement(queryString);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        closeAllAndDisconnect();

        //System.out.println(queryString);
    }

    public static void insert(UserModel userModel) {

        String queryString = "INSERT INTO user (username,password) VALUES ('" + userModel.getUsername() + "','"+userModel.getPassword()+"')";

        try {
            PreparedStatement statement = DBUtil.getPreparedStatement(queryString);
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        closeAllAndDisconnect();

        //System.out.println(queryString);
    }



//    @Test
//    public void test(){
//        BookModel bookModel=new BookModel();
//        AuthorModel authorModel=new AuthorModel();
//        GenreModel genreModel=new GenreModel();
//
//        authorModel.setId(3);
//        authorModel.setFirstName("Author first name111");
//        authorModel.setLastName("Author last name111");
//
//        genreModel.setId(2);
//        genreModel.setName("Relativity physics");
//
//        bookModel.setAuthor(authorModel);
//        bookModel.setGenre(genreModel);
//        bookModel.setId(2);
//        bookModel.setName("Book 101");
//        bookModel.setPublicationDate("1970-01-01");
//
//
//        insert(bookModel);
//    }
}
