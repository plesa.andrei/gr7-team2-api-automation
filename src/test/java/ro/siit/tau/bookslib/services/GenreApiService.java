package ro.siit.tau.bookslib.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ro.siit.tau.bookslib.models.AuthorModel;
import ro.siit.tau.bookslib.models.GenreModel;
import ro.siit.tau.bookslib.tests.BaseTest;

import java.io.IOException;
import java.util.List;
import ro.siit.tau.bookslib.services.*;

public class GenreApiService extends BaseAPI {

    /*public static String getAuthToken() throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(PARTIAL_PATH+"/login");
        httpPost.setEntity(new StringEntity("{\"username\":\"Ioana Pop 1\",\"password\":\"Ioana123\"}"));

        HttpResponse httpResponse = httpClient.execute(httpPost);
        Header authHeader = httpResponse.getFirstHeader("Authorization");
        System.out.println("Token is " + authHeader.getValue());
        return authHeader.getValue();
    }*/



    public static void getGenreList() throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGetMethod = new HttpGet(PARTIAL_PATH+"/genre/list");

        HttpResponse httpResponse = httpClient.execute(httpGetMethod);

        System.out.println(httpResponse.getStatusLine().getStatusCode());
        System.out.println(httpResponse.getStatusLine().getReasonPhrase());

        String responseBody = IOUtils.toString(httpResponse.getEntity().getContent());
        System.out.println(responseBody);

        ObjectMapper mapper = new ObjectMapper();
        List<GenreModel> genres = mapper.readValue(responseBody, new TypeReference<List<GenreModel>>() {
        });

        System.out.println(genres.size() + " genre");

    }

    public static void getGenreId() throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGetMethod = new HttpGet(PARTIAL_PATH+"/genre/show/6");
        HttpResponse httpResponse = httpClient.execute(httpGetMethod);

        System.out.println(httpResponse.getStatusLine().getStatusCode());
        System.out.println(httpResponse.getStatusLine().getReasonPhrase());

        String responseBody = IOUtils.toString(httpResponse.getEntity().getContent());
        System.out.println(responseBody);


    }


    public static void postGenre(GenreModel genreModel) throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(PARTIAL_PATH+"/genre/add");

        BaseAPI.authenticationWithToken(httpPost);
        System.out.println(BaseTest.TOKEN);

        httpPost.setEntity(new StringEntity("{\"name\":\"" + genreModel.getName() + "\"}", ContentType.APPLICATION_JSON));
        System.out.println("{\"name\":\"" + genreModel.getName() + "\"}");
        HttpResponse httpResponse = httpClient.execute(httpPost);

        System.out.println(httpResponse.getStatusLine().getStatusCode());
        System.out.println(httpResponse.getStatusLine().getReasonPhrase());

        String responseBody = IOUtils.toString(httpResponse.getEntity().getContent());
        System.out.println(responseBody);


    }


    public static void putGenre(GenreModel genreModel) throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPut httpPut = new HttpPut(PARTIAL_PATH+"/genre/update/5");

        String token = getAuthToken();
        BasicHeader header = new BasicHeader("Authorization", token);
        httpPut.addHeader(header);

        httpPut.setEntity(new StringEntity("{\"name\":\"" + genreModel.getName() + "\"}", ContentType.APPLICATION_JSON));

        HttpResponse httpResponse = httpClient.execute(httpPut);

        System.out.println(httpResponse.getStatusLine().getStatusCode());
        System.out.println(httpResponse.getStatusLine().getReasonPhrase());

        String responseBody = IOUtils.toString(httpResponse.getEntity().getContent());
        System.out.println(responseBody);

    }



    public static void deleteGenre() throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpDelete httpDelete = new HttpDelete(PARTIAL_PATH+"/genre/delete/5");

        String token = getAuthToken();
        BasicHeader header = new BasicHeader("Authorization", token);
        httpDelete.addHeader(header);

        HttpResponse httpResponse = httpClient.execute(httpDelete);

        System.out.println(httpResponse.getStatusLine().getStatusCode());
        System.out.println(httpResponse.getStatusLine().getReasonPhrase());

        String responseBody = IOUtils.toString(httpResponse.getEntity().getContent());
        System.out.println(responseBody);


    }
}
