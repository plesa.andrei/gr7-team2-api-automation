package ro.siit.tau.bookslib.services;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import ro.siit.tau.bookslib.models.AuthorModel;
import ro.siit.tau.bookslib.models.BookModel;
import ro.siit.tau.bookslib.models.GenreModel;
import ro.siit.tau.bookslib.models.UserModel;

import java.io.IOException;

import static ro.siit.tau.bookslib.tests.BaseTest.*;

public class PostAPI extends BaseAPI {

    public static String post(AuthorModel authorModel) throws IOException {

        String uriText = PARTIAL_PATH+"/author/add";

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(uriText);

        authenticationWithToken(httpPost);


//        String firstNameString = "{\"firstName\":\"" + authorModel.getFirstName() + "\",";
//        String lastNameString = "\"lastName\":\"" + authorModel.getLastName() + "\"}";

        httpPost.setEntity(new StringEntity(authorModelMapper(authorModel), ContentType.APPLICATION_JSON));

        HttpResponse httpResponse = httpClient.execute(httpPost);

        //showResponseInformation(httpResponse);

        return  getResponseString(httpResponse);

    }

    public static String post(BookModel bookModel) throws IOException {

        String uriText = PARTIAL_PATH+"/book/add";

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(uriText);

        authenticationWithToken(httpPost);


//        String firstNameString = "{\"author\":{\"id\":" + bookModel.getAuthor().getId() + ",\"firstName\":\"" + bookModel.getAuthor().getFirstName() + "\",";
//        String lastNameString = "\"lastName\":\"" + bookModel.getAuthor().getFirstName() + "\"},";
//
//
//        String genreNameString = "\"genre\": {\"id\":" + bookModel.getGenre().getId() + ",\"name\":\"" + bookModel.getGenre().getName() + "\"},";
//
//        String bookNameString = "\"id\":" + bookModel.getId() + ",\"name\":\"" + bookModel.getName() + "\",";
//        String publicationDateString = "\"publicationDate\":\"" + bookModel.getPublicationDate() + "\"}";


        httpPost.setEntity(new StringEntity(bookModelMapper(bookModel), ContentType.APPLICATION_JSON));


        HttpResponse httpResponse = httpClient.execute(httpPost);

//        showResponseInformation(httpResponse);

        return  getResponseString(httpResponse);


    }

    public static String post(GenreModel genreModel) throws IOException {

        String uriText = PARTIAL_PATH+"/genre/add";

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpPost httpPost = new HttpPost(uriText);

        authenticationWithToken(httpPost);

        httpPost.setEntity(new StringEntity(genreModelMapper(genreModel), ContentType.APPLICATION_JSON));

        HttpResponse httpResponse = httpClient.execute(httpPost);

        //showResponseInformation(httpResponse);

        return  getResponseString(httpResponse);

    }

    public static String post(UserModel userModel) throws IOException {

        String uriText = PARTIAL_PATH+"/user/add";

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpPost httpPost = new HttpPost(uriText);

        authenticationWithToken(httpPost);

        httpPost.setEntity(new StringEntity(userModelMapper(userModel), ContentType.APPLICATION_JSON));

        HttpResponse httpResponse = httpClient.execute(httpPost);

        //showResponseInformation(httpResponse);

        return  getResponseString(httpResponse);

    }

    public static String post(Table table,String stringForExecute) throws IOException {

        String uriText = "";

        if (table.equals(Table.AUTHOR)) {
            uriText = PARTIAL_PATH+"/author/add";
        }
        if (table.equals(Table.BOOK)) {
            uriText = PARTIAL_PATH+"/book/add";
        }
        if (table.equals(Table.GENRE)) {
            uriText = PARTIAL_PATH+"/genre/add";
        }
        if (table.equals(Table.USER)) {
            uriText = PARTIAL_PATH+"/user/add";
        }

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpPost httpPost = new HttpPost(uriText);

        authenticationWithToken(httpPost);

        httpPost.setEntity(new StringEntity(stringForExecute, ContentType.APPLICATION_JSON));

        HttpResponse httpResponse = httpClient.execute(httpPost);

        //showResponseInformation(httpResponse);

        return  getResponseString(httpResponse);

    }






    public static void post(Table table) throws IOException {

        String uriText = "";

        if (table.equals(Table.AUTHOR)) {
            uriText = PARTIAL_PATH+"/author/add";
        }
        if (table.equals(Table.BOOK)) {
            uriText = PARTIAL_PATH+"/book/add";
        }
        if (table.equals(Table.GENRE)) {
            uriText = PARTIAL_PATH+"/genre/add";
        }
        if (table.equals(Table.USER)) {
            uriText = PARTIAL_PATH+"/user/add";
        }


        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(uriText);

        String token = getAuthToken();
        BasicHeader header = new BasicHeader("Authorization", token);
        httpPost.addHeader(header);

        httpPost.setEntity(new StringEntity("{\"name\":\"SF3 \"}", ContentType.APPLICATION_JSON));


        HttpResponse httpResponse = httpClient.execute(httpPost);

        System.out.println(httpResponse.getStatusLine().getStatusCode());
        System.out.println(httpResponse.getStatusLine().getReasonPhrase());


        //httpResponse.getEntity().getContent();
        String responseBody = IOUtils.toString(httpResponse.getEntity().getContent());
        System.out.println(responseBody);

    }


}
