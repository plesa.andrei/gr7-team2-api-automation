package ro.siit.tau.bookslib.services;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import ro.siit.tau.bookslib.models.AuthorModel;
import ro.siit.tau.bookslib.models.BookModel;
import ro.siit.tau.bookslib.models.GenreModel;

import java.io.IOException;

public class GetAPI extends BaseAPI{

    public static String get(Table table) throws IOException {

        String uriText = "";

        switch (table) {

            case AUTHOR:
                uriText = PARTIAL_PATH+"/author/list";
                break;
            case BOOK:
                uriText = PARTIAL_PATH+"/book/list";
                break;
            case GENRE:
                uriText = PARTIAL_PATH+"/genre/list";
                break;
        }


        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpGet httpGet = new HttpGet(uriText);

        authenticationWithToken(httpGet);


        HttpResponse httpResponse = httpClient.execute(httpGet);

//        showResponseInformation(httpResponse);

        return  getResponseString(httpResponse);


    }

    public static String get(Table table, int id) throws IOException {

        String uriText = "";

        switch (table) {

            case AUTHOR:
                uriText = PARTIAL_PATH+"/author/show/" + id + "";
                break;
            case BOOK:
                uriText = PARTIAL_PATH+"/book/show/" + id + "";
                break;
            case GENRE:
                uriText = PARTIAL_PATH+"/genre/show/" + id + "";
                break;
        }


        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpGet httpGet = new HttpGet(uriText);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        //showResponseInformation(httpResponse);

        return  getResponseString(httpResponse);


    }

    public static void get(AuthorModel authorModel) throws IOException {

        String uriText = PARTIAL_PATH+"/author/list";

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpGet httpGet = new HttpGet(uriText);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        showResponseInformation(httpResponse);

    }

    public static void get(AuthorModel authorModel, int id) throws IOException {

        String uriText = PARTIAL_PATH+"/author/show/" + id + "";

        HttpClient httpClient = HttpClientBuilder.create().build();
//        HttpPost httpPost = new HttpPost(uriText);
        HttpGet httpGet = new HttpGet(uriText);

//        String token = getAuthToken();
//        BasicHeader header = new BasicHeader("Authorization", token);
//        httpGet.addHeader(header);

//        String firstNameString = "{\"firstName\":\"" + authorModel.getFirstName() + "\",";
//        String lastNameString = "\"lastName\":\"" + authorModel.getLastName() + "\"}";

//        httpGet.setEntity(new StringEntity(firstNameString + lastNameString, ContentType.APPLICATION_JSON));

        HttpResponse httpResponse = httpClient.execute(httpGet);

        showResponseInformation(httpResponse);

    }

    public static void get(BookModel bookModel) throws IOException {

        String uriText = PARTIAL_PATH+"/book/list";

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpGet httpGet = new HttpGet(uriText);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        showResponseInformation(httpResponse);

    }

    public static void get(BookModel bookModel, int id) throws IOException {

        String uriText = PARTIAL_PATH+"/book/show/" + id + "";

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpGet httpGet = new HttpGet(uriText);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        showResponseInformation(httpResponse);

    }

    public static void get(GenreModel genreModel) throws IOException {

        String uriText = PARTIAL_PATH+"/genre/list";

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpGet httpGet = new HttpGet(uriText);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        showResponseInformation(httpResponse);

    }

    public static void get(GenreModel genreModel, int id) throws IOException {

        String uriText = PARTIAL_PATH+"/genre/show/" + id + "";

        HttpClient httpClient = HttpClientBuilder.create().build();

        HttpGet httpGet = new HttpGet(uriText);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        showResponseInformation(httpResponse);

    }


}
