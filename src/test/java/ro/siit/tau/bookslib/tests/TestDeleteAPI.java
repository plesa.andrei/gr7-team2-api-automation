package ro.siit.tau.bookslib.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ro.siit.tau.bookslib.models.AuthorModel;
import ro.siit.tau.bookslib.models.BookModel;
import ro.siit.tau.bookslib.models.GenreModel;
import ro.siit.tau.bookslib.services.*;

import java.io.IOException;

import static ro.siit.tau.bookslib.utils.DBUtil.*;

public class TestDeleteAPI extends BaseTest {


    private static AuthorModel prepareAuthorForDelete(){

        AuthorModel authorModel = getRandomAuthorModel(8);

        InsertDB.insert(authorModel);

        return authorModel;

    }

    private  static BookModel prepareBookForDelete(){

        AuthorModel authorModel = getRandomAuthorModel(7);
        GenreModel genreModel = getRandomGenreModel(5);
        BookModel bookModel = getRandomBookModel(6);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
//        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        InsertDB.insert(authorModel);
        InsertDB.insert(genreModel);

        bookModel.getAuthor().setId(nextIndexAuthor);
        bookModel.getGenre().setId(nextIndexGenre);

        InsertDB.insert(bookModel);

        return bookModel;
    }

    private static GenreModel prepareGenreForDelete(){

        GenreModel genreModel = getRandomGenreModel(9);

        InsertDB.insert(genreModel);

        return genreModel;
    }




    @Test
    public static void testDeleteAuthorAPI() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        prepareAuthorForDelete();

        String deleteMessageReceived = DeleteAPI.delete(Table.AUTHOR, nextIndexAuthor);

        System.out.println(deleteMessageReceived);

        // clean from DB the 'AUTHOR'(inserted for test in DB) if it couldn't be deleted by API method from above
        if (validID(Table.AUTHOR, nextIndexAuthor)) {
            cleanModel(Table.AUTHOR,nextIndexAuthor);
            Assert.assertTrue(validID(Table.AUTHOR, nextIndexAuthor), " author with id=" + nextIndexAuthor + " inserted for test  couldn't be  deleted from API and have to be deleted automatic using DB command DELETE so test is FAILED!!!");
        }

        Assert.assertTrue(deleteMessageReceived.contains("Success"), "check if  message received after delete Author from API is 'successful' (message contain 'succes') ");
        Assert.assertFalse(validID(Table.AUTHOR, nextIndexAuthor), "check if (id=" + nextIndexAuthor + ") of author is not present anymore after  delete from API");
    }


    @Test
    public static void testDeleteAuthorWithInvalidID() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        String deleteMessageReceived = DeleteAPI.delete(Table.AUTHOR, nextIndexAuthor);

        System.out.println(deleteMessageReceived);

        Assert.assertTrue(deleteMessageReceived.contains("\"message\":\"Author with id "+nextIndexAuthor+" was not found\""), "check if  message received after delete Author from API  is the one expected ");
//        Assert.assertFalse(validID(Table.AUTHOR, nextIndexAuthor), "check if (id=" + nextIndexAuthor + ") of author is not present anymore after  delete from API");
    }














    @Test
    public static void testDeleteBookAPI() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for clean purpose
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for clean purpose
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        prepareBookForDelete();

        String deleteMessageReceived = DeleteAPI.delete(Table.BOOK, nextIndexBook);

//        System.out.println(deleteMessageReceived);

        // clean from DB the 'BOOK'(inserted for test in DB) if it couldn't be deleted by API method from above
        if (validID(Table.BOOK, nextIndexBook)) {

            cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);

            Assert.assertTrue(validID(Table.BOOK, nextIndexBook), " book with id=" + nextIndexBook + ")inserted for test  couldn't be  deleted from API and have to be deleted automatic using DB command so test is FAILED!!!");
        }

        Assert.assertTrue(deleteMessageReceived.contains("Success"), "check if  message received after delete Book from API is 'successful' ");
        Assert.assertFalse(validID(Table.BOOK, nextIndexBook), "check if (id=" + nextIndexBook + ") of book is not present anymore after  delete from API");

        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);

    }

    @Test
    public static void testDeleteBookWithInvalidID() throws IOException {

        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        String deleteMessageReceived = DeleteAPI.delete(Table.BOOK, nextIndexBook);
//        System.out.println(deleteMessageReceived);
        Assert.assertTrue(deleteMessageReceived.contains("\"message\":\"Book with id "+nextIndexBook+" was not found\""), "check if  message received after delete Author from API  is the one expected ");

    }




    @Test
    public static void testDeleteGenreAPI() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        prepareGenreForDelete();

        String deleteMessageReceived = DeleteAPI.delete(Table.GENRE, nextIndexGenre);
//        System.out.println(deleteMessageReceived);

        //try to clean from DB the 'GENRE'(inserted for test in DB) if it couldn't be deleted by API method from above
        if (validID(Table.GENRE, nextIndexGenre)) {
            cleanModel(Table.GENRE,nextIndexGenre);
            Assert.assertTrue(validID(Table.GENRE, nextIndexGenre), " genre with id=" + nextIndexGenre + ")inserted for test  couldn't be  deleted from API and have to be delete automatic using DB command, so test is FAILED");
        }

        Assert.assertTrue(deleteMessageReceived.contains("Success"), "check if  message received after delete Genre from API is 'successful' ");
        Assert.assertFalse(validID(Table.GENRE, nextIndexGenre), "check if (id=" + nextIndexGenre + ") of genre is not present anymore after  delete from API");

    }

    @Test
    public static void testDeleteGenreWithInvalidID() throws IOException {

        String deleteMessageReceived = DeleteAPI.delete(Table.GENRE, getNextAutoIncrement(Table.GENRE));
//        System.out.println(deleteMessageReceived);
        Assert.assertTrue(deleteMessageReceived.contains("\"message\":\"Genre was not found\""), "check if  message received after try to delete Genre from API is the one expected ");

    }




}
