package ro.siit.tau.bookslib.tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.Test;
import ro.siit.tau.bookslib.models.AuthorModel;
import ro.siit.tau.bookslib.models.BookModel;
import ro.siit.tau.bookslib.models.GenreModel;
import ro.siit.tau.bookslib.models.UserModel;
import ro.siit.tau.bookslib.services.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ro.siit.tau.bookslib.utils.DBUtil.getNextAutoIncrement;

public class TestLoginAPI extends BaseTest {




    @Test
    public static void testLoginUser() throws IOException {

        UserModel userModel=getRandomUserModel(9);

       BaseAPI.addUser(userModel);

        String loginMessage=login(userModel);

        System.out.println(loginMessage);

        Assert.assertEquals(loginMessage,"","check if is no error message when login ");

    }

    @Test
    public static void testLoginUserTwice() throws IOException {

        UserModel userModel=getRandomUserModel(9);

        BaseAPI.addUser(userModel);

        login(userModel);

        String secondLoginMessage=login(userModel);

        System.out.println(secondLoginMessage);

        Assert.assertEquals(secondLoginMessage,"","check if is no error message when login twice ");

    }

    @Test
    public static void testLoginWrongUser() throws IOException {

        UserModel userModel=getRandomUserModel(9);

        BaseAPI.addUser(userModel);

        userModel.setUsername(getRandomText(12));

        String loginMessage=login(userModel);

        System.out.println(loginMessage);

        Assert.assertNotEquals(loginMessage,"","check if  error message appears when login with wrong user name ");

    }
// region Test
    /*test*/
    @Test 
    public static void testLoginWrongPassword() throws IOException {

        UserModel userModel=getRandomUserModel(9);

        BaseAPI.addUser(userModel);

        userModel.setPassword(getRandomText(12));

        String loginMessage=login(userModel);

        System.out.println(loginMessage);

        Assert.assertNotEquals(loginMessage,"","check if  error message appears when login with wrong password ");

    }
    /*test*/
// region Test

}
