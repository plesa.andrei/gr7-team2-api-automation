package ro.siit.tau.bookslib.tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.testng.annotations.*;
import ro.siit.tau.bookslib.models.AuthorModel;
import ro.siit.tau.bookslib.models.BookModel;
import ro.siit.tau.bookslib.models.GenreModel;
import ro.siit.tau.bookslib.models.UserModel;
import ro.siit.tau.bookslib.services.BaseAPI;
import ro.siit.tau.bookslib.services.DeleteDB;
import ro.siit.tau.bookslib.services.Table;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static ro.siit.tau.bookslib.services.BaseAPI.*;
import static ro.siit.tau.bookslib.utils.DBUtil.*;

public class BaseTest {

    public static String baseURL;
    public static String dataBaseURL;
    public static String DATABASE_USER;
    public static String DATABASE_PASSWORD;
    public static String TOKEN;

    public static  int     INITIAL_INDEX_AUTHOR_AUTO_INCREMENT;
    public static  int     INITIAL_INDEX_BOOK_AUTO_INCREMENT;
    public static  int     INITIAL_INDEX_GENRE_AUTO_INCREMENT;
    public static  int     INITIAL_INDEX_USER_AUTO_INCREMENT;


    @Parameters({"commonPath"})
    @BeforeSuite
    public void beforeTest(@Optional("vm") String firstPartialPath) {

        if (firstPartialPath.equals("vm")) {

                baseURL = "http://127.0.0.1:6868/bookslib-rest";
                dataBaseURL = "://127.0.0.1:3316";
                DATABASE_USER = "remote";
                DATABASE_PASSWORD = "remote";}

        else if(firstPartialPath.equals("vmIoana")) {
            baseURL = "http://localhost:8080";
            dataBaseURL = "://localhost:3306";
            DATABASE_USER="root";
            DATABASE_PASSWORD="root";

        }


        try {
            BaseAPI.addUser();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            TOKEN=getAuthToken();
        } catch (IOException e) {
            e.printStackTrace();
        }

        BaseTest.INITIAL_INDEX_AUTHOR_AUTO_INCREMENT=getNextAutoIncrement(Table.AUTHOR);
        BaseTest.INITIAL_INDEX_BOOK_AUTO_INCREMENT=getNextAutoIncrement(Table.BOOK);
        BaseTest.INITIAL_INDEX_GENRE_AUTO_INCREMENT=getNextAutoIncrement(Table.GENRE);
        BaseTest.INITIAL_INDEX_USER_AUTO_INCREMENT=getNextAutoIncrement(Table.USER);

    }


    @AfterSuite
    public void afterSuite(){
        cleanAllModelsInsertedForTest();
    }

    @BeforeTest
    public void beforeEachTest() {

    }

    @AfterTest
    public void afterEachTest() {

    }

    public static String getHostName (){

        String hostname = "Unknown";

        try
        {
            InetAddress addr = InetAddress.getLocalHost();
            hostname = addr.getHostName();
        }
        catch (UnknownHostException ex)
        {
            System.out.println("Hostname can not be resolved");
        }
        System.out.println("host name:"+hostname);
        return hostname;
    }

    public static String  login(UserModel userModel) throws IOException {

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(baseURL+"/login");
        authenticationWithToken(httpPost);


        httpPost.setEntity(new StringEntity("{\"username\":\""+userModel.getUsername()+"\",\"password\":\""+userModel.getPassword()+"\"}"));

        HttpResponse httpResponse = httpClient.execute(httpPost);
//        Header authHeader = httpResponse.getFirstHeader("Authorization");
//
//        System.out.println("token is:" + authHeader.getValue());

        return IOUtils.toString(httpResponse.getEntity().getContent());
    }

    public static String getRandomText(int n)   {

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvwxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int)(AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

    public static int getRandomNumber()   {

        int n=999999;

        return (int) (n* Math.random());
    }

    public static int getRandomNumber(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static AuthorModel getRandomAuthorModel(int length){

        AuthorModel authorModel=new AuthorModel();

        authorModel.setId(getRandomNumber());
        authorModel.setFirstName(getRandomText(length)); // random text method
        authorModel.setLastName(getRandomText(length));

        return authorModel;
    }

    public static BookModel getRandomBookModel(int length){

        BookModel bookModel=new BookModel();
        AuthorModel authorModel=getRandomAuthorModel(length);
        GenreModel genreModel=getRandomGenreModel(length);

        bookModel.setAuthor(authorModel);
        bookModel.setGenre(genreModel);
        bookModel.setId(getRandomNumber());
        bookModel.setName(getRandomText(length));
        bookModel.setPublicationDate(getRandomNumber(1970,2019)+"-"+getRandomNumber(1,12)+"-"+getRandomNumber(1,27));

        return bookModel;
    }

    public static GenreModel getRandomGenreModel(int length){

        GenreModel genreModel=new GenreModel();

        genreModel.setId(getRandomNumber());
        genreModel.setName(getRandomText(length));

        return genreModel;
    }

    public static UserModel getRandomUserModel(int length){

        if(length<8){length=9;}//for min 8 char for password

        UserModel userModel=new UserModel();

        userModel.setId(getRandomNumber());
        userModel.setUsername(getRandomText(length));
        userModel.setPassword(getRandomText(length));

        return userModel;
    }

    public static boolean modelsHaveEqualValues(AuthorModel authorModelFirst,AuthorModel authorModelSecond){

        if (authorModelFirst.getId()!=authorModelSecond.getId()){System.out.println("author-id not equal");return false; }

        if (!authorModelFirst.getFirstName().equals(authorModelSecond.getFirstName())){System.out.println("author-fname  not equal: "+authorModelFirst.getFirstName()+"and:"+authorModelSecond.getFirstName());return false; }
        if (!authorModelFirst.getLastName().equals(authorModelSecond.getLastName())){System.out.println("author-lname  not equal");return false; }

        return true;
    }

    public static boolean modelsHaveEqualValues(BookModel bookModelFirst,BookModel bookModelSecond){

        if(!modelsHaveEqualValues(bookModelFirst.getAuthor(),bookModelSecond.getAuthor())){System.out.println("");return false; }
        if(!modelsHaveEqualValues(bookModelFirst.getGenre(),bookModelSecond.getGenre())){System.out.println("");return false;}

        if (bookModelFirst.getId()!=bookModelSecond.getId()){System.out.println("id book not equal");return false;}

        if (!bookModelFirst.getName().equals(bookModelSecond.getName())){System.out.println("book-name not equal");return false; }
        if (!equalDate(bookModelFirst.getPublicationDate(),bookModelSecond.getPublicationDate())){System.out.println("publication_date not equal");return false; }

        return true;
    }

    public static boolean modelsHaveEqualValues(GenreModel genreModelFirst,GenreModel genreModelSecond){

        if (genreModelFirst.getId()!=genreModelSecond.getId()){System.out.println("genre-id not equal");return false; }
        if (!genreModelFirst.getName().equals(genreModelSecond.getName())){System.out.println("genre-name  not equal");return false; }

        return true;
    }

    public static boolean modelsHaveEqualValues(UserModel userModelFirst,UserModel userModelSecond){

        if (userModelFirst.getId()!=userModelSecond.getId()){System.out.println("user-id not equal");return false; }

        if (!userModelFirst.getUsername().equals(userModelSecond.getUsername())){System.out.println("username  not equal: "+userModelFirst.getUsername()+"and:"+userModelSecond.getUsername());return false; }
//        if (!userModelFirst.getPassword().equals(userModelSecond.getPassword())){System.out.println("user-password  not equal");return false; }

        return true;
    }

    public static boolean equalDate(String firstDate,String secondDate) {

        String firstYear, firstMonth, firstDay,
                secondYear, secondMonth, secondDay;

        firstYear = firstDate.substring(0, firstDate.indexOf("-", 3));
        firstMonth = firstDate.substring(firstDate.indexOf("-", 3) + 1, firstDate.lastIndexOf("-"));
        if (firstDate.lastIndexOf("-") + 3 <= firstDate.length()) {
            firstDay = firstDate.substring(firstDate.lastIndexOf("-") + 1, firstDate.lastIndexOf("-") + 3);
        } else {
            firstDay = firstDate.substring(firstDate.lastIndexOf("-") + 1, firstDate.length());
        }

        secondYear = secondDate.substring(0, secondDate.indexOf("-", 3));
        secondMonth = secondDate.substring(secondDate.indexOf("-", 3) + 1, secondDate.lastIndexOf("-"));
        if (secondDate.lastIndexOf("-") + 3 <= secondDate.length()) {
            secondDay = secondDate.substring(secondDate.lastIndexOf("-") + 1, secondDate.lastIndexOf("-") + 3);
        } else {
            secondDay = secondDate.substring(secondDate.lastIndexOf("-") + 1, secondDate.length());
        }


        if (Integer.parseInt(firstYear) != Integer.parseInt(secondYear)) {
            return false;
        }
        if (Integer.parseInt(firstMonth) != Integer.parseInt(secondMonth)) {
            return false;
        }
        if (Integer.parseInt(firstDay) != Integer.parseInt(secondDay)) {
            return false;
        }

        return true;
    }

    public static void printOneDataModels(Object object){

        List<Object> list=new ArrayList<>();

        list.add(object);

        printAllDataModels(list);

    }

    public  static void printAllDataModels(List<Object> list){

        Table table=Table.USER;

        if (list.size()>0) {

            if (list.get(0) instanceof AuthorModel) {
                table = Table.AUTHOR;
            } else if (list.get(0) instanceof BookModel) {
                table = Table.BOOK;
            } else if (list.get(0) instanceof GenreModel) {
                table = Table.GENRE;
            }
        }
        else {
            System.out.println("There is nothing in the list to print");
        }

        switch (table) {
            case AUTHOR:
                System.out.println("Total of "+getDBCount(Table.AUTHOR)+" authors founded ");

                for (int i = 0; i <list.size() ; i++) {

                    System.out.print(" id= "+ ((AuthorModel)(list.get(i))).getId() );
                    System.out.print(" fname= "+ ((AuthorModel)(list.get(i))).getFirstName() );
                    System.out.print(" lname= "+ ((AuthorModel)(list.get(i))).getLastName() );

                    System.out.println();
                }
                break;

            case BOOK:
                System.out.println( "Total of "+getDBCount(Table.BOOK)+" Books founded :");

                for (int i = 0; i <list.size() ; i++) {

                    System.out.print(" id= "+ BookModel.class.cast(list.get(i)).getId() );
                    System.out.print(" name= "+ BookModel.class.cast(list.get(i)).getName() );
                    System.out.print(" publication date= "+ BookModel.class.cast(list.get(i)).getPublicationDate() );
                    System.out.print(" author id= "+ BookModel.class.cast(list.get(i)).getAuthor().getId() );
                    System.out.print(" genre id= "+ BookModel.class.cast(list.get(i)).getGenre().getId() );

                    System.out.println();
                }
                break;
            case GENRE:
                System.out.println("Total of "+getDBCount(Table.GENRE)+" Genres in DB :");

                for (int i = 0; i <list.size() ; i++) {

                    System.out.print(" id= "+ ((GenreModel)(list.get(i))).getId() );
                    System.out.print(" name= "+ ((GenreModel)(list.get(i))).getName() );

                    System.out.println();
                }
                break;
            case USER:
                System.out.println("Total of "+getDBCount(Table.USER)+" users founded ");

                for (int i = 0; i <list.size() ; i++) {

                    System.out.print(" id= "+ ((UserModel)(list.get(i))).getId() );
                    System.out.print(" userName= "+ ((UserModel)(list.get(i))).getUsername() );
                    System.out.print(" password= "+ ((UserModel)(list.get(i))).getPassword() );

                    System.out.println();
                }
                break;
        }


    }

    public static String authorModelMapper(AuthorModel authorModel) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
//        String authorMapper = mapper.writeValueAsString(authorModel);

        return mapper.writeValueAsString(authorModel);

    }

    public static String bookModelMapper(BookModel bookModel) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
//        String bookMapper = mapper.writeValueAsString(bookModel);

        return mapper.writeValueAsString(bookModel);

    }

    public static String genreModelMapper(GenreModel genreModel) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
//        String genreMapper = mapper.writeValueAsString(genreModel);

        return mapper.writeValueAsString(genreModel);

    }

    public static String userModelMapper(UserModel userModel) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
//        String genreMapper = mapper.writeValueAsString(genreModel);

        return mapper.writeValueAsString(userModel);

    }

    public static void cleanModel(Table table,int indexTable){

        switch (table){
            case AUTHOR:
                if (validID(Table.AUTHOR, indexTable)) {
                    DeleteDB.delete(Table.AUTHOR, indexTable);
                }
                break;
            case BOOK:
                if (validID(Table.BOOK, indexTable)) {
                DeleteDB.delete(Table.BOOK, indexTable);
            }
                break;
            case GENRE:
                if (validID(Table.GENRE, indexTable)) {
                    DeleteDB.delete(Table.GENRE, indexTable);
                }
                break;
            case USER:
                if (validID(Table.USER, indexTable)) {
                    DeleteDB.delete(Table.USER, indexTable);
                }
                break;

        }

    }

    public static void cleanModel(Table book,int indexBook,Table authorOrGenre,int indexAuthorOrGenre,Table genreOrAuthor, int indexGenreOrAuthor ){

        if (book==Table.BOOK){cleanModel(Table.BOOK,indexBook);}
        else{
            System.out.println("cannot clean because is not a book model ");
        }

        if (authorOrGenre == Table.AUTHOR) {
            cleanModel(Table.AUTHOR, indexAuthorOrGenre);
        } else {
            if (authorOrGenre == Table.GENRE) {
                cleanModel(Table.GENRE, indexAuthorOrGenre);
            } else
                System.out.println("cannot clean because is not a author or genre model ");
        }

        if (genreOrAuthor == Table.AUTHOR) {
            cleanModel(Table.AUTHOR, indexGenreOrAuthor);
        } else {
            if (genreOrAuthor == Table.GENRE) {
                cleanModel(Table.GENRE, indexGenreOrAuthor);
            } else
                System.out.println("cannot clean because is not a genre or author model ");
        }


    }

    public static void cleanAllModelsInsertedForTest(){

        for (int i = INITIAL_INDEX_BOOK_AUTO_INCREMENT; i <getNextAutoIncrement(Table.BOOK) ; i++) {
            cleanModel(Table.BOOK,i);
        }

        for (int i = INITIAL_INDEX_AUTHOR_AUTO_INCREMENT; i <getNextAutoIncrement(Table.AUTHOR) ; i++) {
            cleanModel(Table.AUTHOR,i);
        }

        for (int i = INITIAL_INDEX_GENRE_AUTO_INCREMENT; i <getNextAutoIncrement(Table.GENRE) ; i++) {
            cleanModel(Table.GENRE,i);
        }

        for (int i = INITIAL_INDEX_USER_AUTO_INCREMENT; i <getNextAutoIncrement(Table.USER) ; i++) {
            cleanModel(Table.USER,i);
        }




    }

    public static String transformAuthorStringForExecute(AuthorModel authorModel,String field,String newValueStringWithNoBrakets){

        String firstNameString = "{\"firstName\":\"" + authorModel.getFirstName() + "\",";
        String idString = "\"id\":" + authorModel.getId() + ",";
        String lastNameString = "\"lastName\":\"" + authorModel.getLastName() + "\"}";

        switch (field.toLowerCase()) {

            case "firstname":

                 firstNameString = "{\"firstName\":" + newValueStringWithNoBrakets + ",";
                 break;

            case "id":

                idString = "\"id\":" + newValueStringWithNoBrakets + ",";
                break;

            case "lastname":

                lastNameString = "\"lastName\":" + newValueStringWithNoBrakets + "}";
                break;
        }

       return firstNameString+idString+lastNameString;
    }

    public static String transformGenreStringForExecute(GenreModel genreModel,String field,String newValueStringWithNoBrakets){

        String nameString = "{\"name\":\"" + genreModel.getName() + "\",";
        String idString = "\"id\":" + genreModel.getId() + "}";

        switch (field.toLowerCase()) {

            case "name":

                nameString = "{\"name\":" + newValueStringWithNoBrakets + ",";
                break;

            case "id":

                idString = "\"id\":" + newValueStringWithNoBrakets + "}";
                break;
        }

        return nameString+idString;
    }

    public static String transformBookStringForExecute(BookModel bookModel,String field,String newValueStringWithNoBrakets){

         String firstNameString = "{\"author\":{\"id\":" + bookModel.getAuthor().getId() + ",\"firstName\":\"" + bookModel.getAuthor().getFirstName() + "\",";
         String lastNameString = "\"lastName\":\"" + bookModel.getAuthor().getLastName() + "\"},";


         String genreNameString = "\"genre\": {\"id\":" + bookModel.getGenre().getId() + ",\"name\":\"" + bookModel.getGenre().getName() + "\"},";

         String bookNameString = "\"id\":" + bookModel.getId() + ",\"name\":\"" + bookModel.getName() + "\",";
         String publicationDateString = "\"publicationDate\":\"" + bookModel.getPublicationDate() + "\"}";

        switch (field.toLowerCase()) {

            case "authorid":
                firstNameString = "{\"author\":{\"id\":" + newValueStringWithNoBrakets + ",\"firstName\":\"" + bookModel.getAuthor().getFirstName() + "\",";
                break;

            case "authorfirstname":
                firstNameString = "{\"author\":{\"id\":" + bookModel.getAuthor().getId() + ",\"firstName\":" + newValueStringWithNoBrakets + ",";
                break;
            case "authorlastname":
                lastNameString = "\"lastName\":" + newValueStringWithNoBrakets + "},";
                break;
            case "genreid":
                genreNameString = "\"genre\": {\"id\":" + newValueStringWithNoBrakets + ",\"name\":\"" + bookModel.getGenre().getName() + "\"},";
                break;
            case "genrename":
                genreNameString = "\"genre\": {\"id\":" + bookModel.getGenre().getId() + ",\"name\":" + newValueStringWithNoBrakets + "},";
                break;
            case "id":
                bookNameString = "\"id\":" + newValueStringWithNoBrakets + ",\"name\":\"" + bookModel.getName() + "\",";
                break;
            case "name":
                bookNameString = "\"id\":" + bookModel.getId() + ",\"name\":" + newValueStringWithNoBrakets + ",";
                break;
            case "publicationdate":
                publicationDateString = "\"publicationDate\":" + newValueStringWithNoBrakets + "}";
                break;
        }

        return firstNameString+lastNameString+genreNameString+bookNameString+publicationDateString;
    }


}
