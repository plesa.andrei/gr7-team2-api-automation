package ro.siit.tau.bookslib.tests;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;
import ro.siit.tau.bookslib.models.AuthorModel;
import ro.siit.tau.bookslib.models.BookModel;
import ro.siit.tau.bookslib.models.GenreModel;

import java.io.IOException;

import ro.siit.tau.bookslib.models.UserModel;
import  ro.siit.tau.bookslib.services.*;

import static ro.siit.tau.bookslib.utils.DBUtil.*;


public class TestPostAPI extends BaseTest{

    private static AuthorModel prepareAuthorForPost(){

        AuthorModel authorModel = getRandomAuthorModel(8);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);


        authorModel.setId(nextIndexAuthor);

        return authorModel;

    }

    private static BookModel prepareBookForPost(){
        AuthorModel authorModel = getRandomAuthorModel(7);
        GenreModel genreModel = getRandomGenreModel(5);
        BookModel bookModel = getRandomBookModel(6);


        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        InsertDB.insert(authorModel);
        InsertDB.insert(genreModel);

        authorModel.setId(nextIndexAuthor);
        genreModel.setId(nextIndexGenre);

        bookModel.setAuthor(authorModel);
        bookModel.setGenre(genreModel);
        bookModel.setId(nextIndexBook);

        return bookModel;
    }

    private static GenreModel prepareGenreForPost(){

        GenreModel genreModel = getRandomGenreModel(8);

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        genreModel.setId(nextIndexGenre);

        return genreModel;

    }

    private static UserModel prepareUserForPost(){

        UserModel userModel = getRandomUserModel(10);

        int nextIndexUser = getNextAutoIncrement(Table.USER);

        userModel.setId(nextIndexUser);

        return userModel;

    }

    @Test
    public static void testPostAuthor() throws IOException {

//        AuthorModel authorModel = getRandomAuthorModel(8);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        int countBeforePost = getDBCount(Table.AUTHOR);

        AuthorModel authorModel=prepareAuthorForPost();

        String postMessageReceived = PostAPI.post(authorModel);

        int countAfterPost = getDBCount(Table.AUTHOR);//always after post method//

        AuthorModel authorModelGetFromDB = (AuthorModel)(SelectDB.select(Table.AUTHOR, nextIndexAuthor).get(0));


        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Author from API is successful ");

        Assert.assertEquals(countAfterPost, countBeforePost + 1, "check if DB number of " + Table.AUTHOR + " records after POST is 1 greater than before POST ");

        Assert.assertTrue(modelsHaveEqualValues(authorModel, authorModelGetFromDB), "check if the random data model POST-ed through API is also  find in DB");

//        System.out.println("message" + postMessageReceived);

        cleanModel(Table.AUTHOR, nextIndexAuthor);


    }


    @Test
    public static void testPostAuthorWithExistentID() throws IOException {

        int indexToBeImposed=getNextAutoIncrement(Table.AUTHOR);

        AuthorModel authorModel = prepareAuthorForPost();

        InsertDB.insert(authorModel);

        AuthorModel authorModelForPost=getRandomAuthorModel(9);

        authorModelForPost.setId(indexToBeImposed);

        int indexToBePosted=getNextAutoIncrement(Table.AUTHOR);


        String postMessageReceived = PostAPI.post(authorModelForPost);

        AuthorModel authorModelGetFromDB = (AuthorModel)(SelectDB.select(Table.AUTHOR, indexToBePosted).get(0));

        authorModelForPost.setId(indexToBeImposed+1);
//        printOneDataModels(authorModelForPost);
//        printOneDataModels(authorModelGetFromDB);

        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Author from API is successful ");

        Assert.assertTrue(modelsHaveEqualValues(authorModelForPost, authorModelGetFromDB), "check if the random data model POST-ed through API is also  find in DB");

        cleanModel(Table.AUTHOR, indexToBeImposed);
        cleanModel(Table.AUTHOR, indexToBePosted);

    }

    @Test
    public static void testPostAuthorWithNullFirstName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        AuthorModel authorModel=prepareAuthorForPost();

        authorModel.setFirstName(null);

        String postMessageReceived = PostAPI.post(authorModel);
//        System.out.println(postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Missing first name\""), "check if  message received after POST-ing Author from API is the one expected ");

        // clean from DB the 'AUTHOR' if is POST-ed by mistake by the test and fail the assert test
        if (validID(Table.AUTHOR, nextIndexAuthor)) {

            boolean authorExists=validID(Table.AUTHOR, nextIndexAuthor);

            cleanModel(Table.AUTHOR, nextIndexAuthor);

            Assert.assertFalse(authorExists, "  POST-ing by mistake Author from API with missing First Name(fname=null) ");
        }

    }

    @Test
    public static void testPostAuthorWithNullLastName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        AuthorModel authorModel=prepareAuthorForPost();

        authorModel.setLastName(null);

        String postMessageReceived = PostAPI.post(authorModel);

        System.out.println(postMessageReceived);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Missing last name\""), "check if  message received after POST-ing Author from API is the one expected ");

        // clean from DB the 'AUTHOR' if is POST-ed by mistake by the test and fail the assert test
        if (validID(Table.AUTHOR, nextIndexAuthor)) {

            boolean authorExists=validID(Table.AUTHOR, nextIndexAuthor);

            cleanModel(Table.AUTHOR, nextIndexAuthor);

            Assert.assertFalse(authorExists, "  POST-ing by mistake Author from API with missing Last Name(lname=null) ");
        }

    }

    @Test
        public static void testPostAuthorWithNullID() throws IOException {

        AuthorModel authorModel = getRandomAuthorModel(8);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        String stringID = "null";

        String stringAuthor = transformAuthorStringForExecute(authorModel, "id", stringID);
//        System.out.println(stringAuthor);
        String postMessageReceived = PostAPI.post(Table.AUTHOR, stringAuthor);
//        System.out.println(postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Author with null ID from API is successful ");

        cleanModel(Table.AUTHOR, nextIndexAuthor);

    }

    @Test
    public static void testPostAuthorWithEmptyFirstName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        AuthorModel authorModel=prepareAuthorForPost();

        authorModel.setFirstName("");

        String postMessageReceived = PostAPI.post(authorModel);

//        System.out.println(postMessageReceived);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"The author first name should have at least 1 character\""), "check if  message received after POST-ing Author from API is the one expected ");

        // clean from DB the 'AUTHOR' if is POST-ed by mistake by the test and fail the assert test
        if (validID(Table.AUTHOR, nextIndexAuthor)) {

            boolean authorExists=validID(Table.AUTHOR, nextIndexAuthor);

            cleanModel(Table.AUTHOR, nextIndexAuthor);

            Assert.assertFalse(authorExists, "  POST-ing by mistake Author from API with empty First Name ");
        }

    }

    @Test
    public static void testPostAuthorWithEmptyLastName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        AuthorModel authorModel=prepareAuthorForPost();

        authorModel.setLastName("");

        String postMessageReceived = PostAPI.post(authorModel);

//        System.out.println(postMessageReceived);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"The author last name should have at least 1 character\""), "check if  message received after POST-ing Author from API is the one expected ");

        // clean from DB the 'AUTHOR' if is POST-ed by mistake by the test and fail the assert test
        if (validID(Table.AUTHOR, nextIndexAuthor)) {

            boolean authorExists=validID(Table.AUTHOR, nextIndexAuthor);

            cleanModel(Table.AUTHOR, nextIndexAuthor);

            Assert.assertFalse(authorExists, "  POST-ing by mistake Author from API with empty Last Name ");
        }

    }

    @Test
    public static void testPostAuthorWithLongerFirstName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        AuthorModel authorModel=prepareAuthorForPost();

        authorModel.setFirstName("more 45 characters 12345678901234567890123456789012345678901234567890");

        String postMessageReceived = PostAPI.post(authorModel);

        System.out.println(postMessageReceived);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"The author first name MAX length is 45\""), "check if  message received after POST-ing Author from API is the one expected ");

        // clean from DB the 'AUTHOR' if is POST-ed by mistake by the test and fail the assert test
        if (validID(Table.AUTHOR, nextIndexAuthor)) {

            boolean authorExists=validID(Table.AUTHOR, nextIndexAuthor);

            cleanModel(Table.AUTHOR, nextIndexAuthor);

            Assert.assertFalse(authorExists, "  POST-ing by mistake Author from API with longer First Name ");
        }

    }

    @Test
    public static void testPostAuthorWithLongerID() throws IOException {

        AuthorModel authorModel = getRandomAuthorModel(8);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        String stringID = "12345678901234567890";

        String stringAuthor = transformAuthorStringForExecute(authorModel, "id", stringID);
//        System.out.println(stringAuthor);
        String postMessageReceived = PostAPI.post(Table.AUTHOR, stringAuthor);
//        System.out.println(postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"JSON parse error: Numeric value (" + stringID + ") out of range of int"), "check if  message received after POST-ing Author from API is the one expected ");

        // clean from DB the 'AUTHOR' if is POST-ed by mistake by the test and fail the assert test
        if (validID(Table.AUTHOR, nextIndexAuthor)) {

            boolean authorExists = validID(Table.AUTHOR, nextIndexAuthor);

            cleanModel(Table.AUTHOR, nextIndexAuthor);

            Assert.assertFalse(authorExists, "  POST-ing by mistake Author from API with longer First Name ");
        }

    }



    @Test
    public static void testPostAuthorWithLongerLastName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        AuthorModel authorModel=prepareAuthorForPost();

        authorModel.setLastName("more 45 characters 12345678901234567890123456789012345678901234567890");

        String postMessageReceived = PostAPI.post(authorModel);

        System.out.println(postMessageReceived);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"The author last name MAX length is 45\""), "check if  message received after POST-ing Author from API is the one expected ");

        // clean from DB the 'AUTHOR' if is POST-ed by mistake by the test and fail the assert test
        if (validID(Table.AUTHOR, nextIndexAuthor)) {

            boolean authorExists=validID(Table.AUTHOR, nextIndexAuthor);

            cleanModel(Table.AUTHOR, nextIndexAuthor);

            Assert.assertFalse(authorExists, "  POST-ing by mistake Author from API with longer Last Name ");
        }

    }


    @Test
    public static void testPostAuthorTwice() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        AuthorModel authorModel=prepareAuthorForPost();

        InsertDB.insert(authorModel);

        String postMessageReceived = PostAPI.post(authorModel);
//        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Author already exists!\""), "check if  message received after trying to POST again the same Author from API is the one expected ");

        // clean from DB the 'AUTHOR' POST-ed by the test
        cleanModel(Table.AUTHOR,nextIndexAuthor);
    }

    @Test
    public static void testPostAuthorWithInvalidIDWithNoNumber() throws IOException {

        AuthorModel authorModel = getRandomAuthorModel(8);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        String stringID = "abc"+getRandomText(6);//first character must not be a number

        String stringAuthor = transformAuthorStringForExecute(authorModel, "id", stringID);
        System.out.println(stringAuthor);
        String postMessageReceived = PostAPI.post(Table.AUTHOR, stringAuthor);
        System.out.println(postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"JSON parse error: Unrecognized token '"+stringID+"'"), "check if  message received after POST-ing Author with invalid ID from API is the one expected ");

        if (validID(Table.AUTHOR, nextIndexAuthor)) {

            boolean authorExists=validID(Table.AUTHOR, nextIndexAuthor);

            cleanModel(Table.AUTHOR, nextIndexAuthor);

            Assert.assertFalse(authorExists, "  POST-ing by mistake Author from API with invalid ID ");
        }
    }

    @Test
    public static void testPostAuthorWithInvalidIDAsString() throws IOException {

        AuthorModel authorModel = getRandomAuthorModel(8);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        String stringID = "\""+getRandomText(9)+"\"";

        String stringAuthor = transformAuthorStringForExecute(authorModel, "id", stringID);
        System.out.println(stringAuthor);
        String postMessageReceived = PostAPI.post(Table.AUTHOR, stringAuthor);
//        System.out.println(postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"JSON parse error: Can not deserialize value of type int from String"), "check if  message received after POST-ing Author with invalid ID from API is the one expected ");

        if (validID(Table.AUTHOR, nextIndexAuthor)) {

            boolean authorExists=validID(Table.AUTHOR, nextIndexAuthor);

            cleanModel(Table.AUTHOR, nextIndexAuthor);

            Assert.assertFalse(authorExists, "  POST-ing by mistake Author from API with invalid ID ");
        }
    }

    @Test
    public static void testPostAuthorWithValidFirstNameAsInteger() throws IOException {

        AuthorModel authorModel = getRandomAuthorModel(8);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        String stringFirstName = ""+getRandomNumber(1,999999999);

        String stringAuthor = transformAuthorStringForExecute(authorModel, "firstName", stringFirstName);
        System.out.println(stringAuthor);
        String postMessageReceived = PostAPI.post(Table.AUTHOR, stringAuthor);
        System.out.println(postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Author with valid first name(insert as valid integer) from API is successful ");

        cleanModel(Table.AUTHOR, nextIndexAuthor);
    }

    @Test
    public static void testPostAuthorWithValidLastNameAsInteger() throws IOException {

        AuthorModel authorModel = getRandomAuthorModel(8);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        String stringLastName = ""+getRandomNumber(1,999999999);

        String stringAuthor = transformAuthorStringForExecute(authorModel, "lastName", stringLastName);
        System.out.println(stringAuthor);
        String postMessageReceived = PostAPI.post(Table.AUTHOR, stringAuthor);
        System.out.println(postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Author with valid last name(insert as valid integer) from API is successful ");

        cleanModel(Table.AUTHOR, nextIndexAuthor);
    }

    @Test
    public static void testPostAuthorWithValidIDAsStringInteger() throws IOException {

        AuthorModel authorModel = getRandomAuthorModel(8);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        String stringID = "\""+getRandomNumber(99,9999)+"\"";

        String stringAuthor = transformAuthorStringForExecute(authorModel, "id", stringID);
        System.out.println(stringAuthor);
        String postMessageReceived = PostAPI.post(Table.AUTHOR, stringAuthor);
        System.out.println(postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Author with invalid ID(insert as valid String) from API is successful ");

        cleanModel(Table.AUTHOR, nextIndexAuthor);
    }










    @Test
    public static void testPostBook() throws IOException {


        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        int countBeforePost = getDBCount(Table.BOOK);

        BookModel bookModel=prepareBookForPost();

        String postMessageReceived = PostAPI.post(bookModel);

        int countAfterPost = getDBCount(Table.BOOK);//always after post method//

        BookModel bookModelGetFromDB = (BookModel)(SelectDB.select(Table.BOOK, nextIndexBook).get(0));

        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Book from API is successful ");

        Assert.assertEquals(countAfterPost, countBeforePost + 1, "check if DB number of " + Table.BOOK + " records after POST is 1 greater than before POST ");

        Assert.assertTrue(modelsHaveEqualValues(bookModel, bookModelGetFromDB), "check if the random data model POST-ed through API is also  find in DB");

        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);

    }

    @Test
    public static void testPostBookWithExistentID() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel=prepareBookForPost();

        printOneDataModels(bookModel);


        InsertDB.insert(bookModel);//insert book in DB for the test

        int nextIndexAuthorForPost = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenreForPost = getNextAutoIncrement(Table.GENRE);
        int nextIndexBookForPost = getNextAutoIncrement(Table.BOOK);

        BookModel bookModelForPost=prepareBookForPost();

        bookModelForPost.setId(nextIndexBook);

//        printOneDataModels(bookModelForPost);

        int countBeforePost = getDBCount(Table.BOOK);


        String postMessageReceived = PostAPI.post(bookModelForPost);

        int countAfterPost = getDBCount(Table.BOOK);//always after post method//

        BookModel bookModelGetFromDB = (BookModel)(SelectDB.select(Table.BOOK, nextIndexBookForPost).get(0));

        bookModelForPost.setId(nextIndexBook+1);


        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Book from API is successful ");
        Assert.assertEquals(countAfterPost, countBeforePost + 1, "check if DB number of " + Table.BOOK + " records after POST is 1 greater than before POST ");
        Assert.assertTrue(modelsHaveEqualValues(bookModelForPost, bookModelGetFromDB), "check if the random data model POST-ed through API is also  find in DB");

        cleanModel(Table.BOOK,nextIndexBookForPost,Table.AUTHOR,nextIndexAuthorForPost,Table.GENRE,nextIndexGenreForPost);
        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);

    }

    @Test
    public static void testPostBookTwice() throws IOException {


        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel=prepareBookForPost();

        printOneDataModels(bookModel);

        InsertDB.insert(bookModel);//insert book in DB for the test

        int nextIndexAuthorForPost = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenreForPost = getNextAutoIncrement(Table.GENRE);
        int nextIndexBookForPost = getNextAutoIncrement(Table.BOOK);

        BookModel bookModelForPost=bookModel;

//        printOneDataModels(bookModelForPost);

        int countBeforePost = getDBCount(Table.BOOK);


        String postMessageReceived = PostAPI.post(bookModelForPost);

        int countAfterPost = getDBCount(Table.BOOK);//always after post method//

        BookModel bookModelGetFromDB = (BookModel)(SelectDB.select(Table.BOOK, nextIndexBookForPost).get(0));

        bookModelForPost.setId(nextIndexBook+1);//for no.3 assert


        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Book from API is successful ");
        Assert.assertEquals(countAfterPost, countBeforePost + 1, "check if DB number of " + Table.BOOK + " records after POST is 1 greater than before POST ");
        Assert.assertTrue(modelsHaveEqualValues(bookModelForPost, bookModelGetFromDB), "check if the random data model POST-ed through API is also  find in DB");

        cleanModel(Table.BOOK,nextIndexBookForPost,Table.AUTHOR,nextIndexAuthorForPost,Table.GENRE,nextIndexGenreForPost);
        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);

    }

    @Test
    public static void testPostBookWithInvalidAuthorID() throws IOException {


        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for clean purpose
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for clean purpose
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel=prepareBookForPost();

        cleanModel(Table.AUTHOR,nextIndexAuthor);

        String postMessageReceived = PostAPI.post(bookModel);

        System.out.println(postMessageReceived);

        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Cannot add or update a child row: a foreign key constraint fails"), "check if  message received after try POST-ing Book from API is the one expected ");

    }

    @Test
    public static void testPostBookWithInvalidGenreID() throws IOException {


        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for clean purpose
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for clean purpose
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel=prepareBookForPost();

        cleanModel(Table.GENRE,nextIndexGenre);

        String postMessageReceived = PostAPI.post(bookModel);

        System.out.println(postMessageReceived);

        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Cannot add or update a child row: a foreign key constraint fails"), "check if  message received after try POST-ing Book from API is the one expected ");

    }




    @Test
    public static void testPostBookWithNullAuthor() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        bookModel.setAuthor(null);

        String postMessageReceived = PostAPI.post(bookModel);
//        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Missing author\""), "check if  message received after POST-ing Book from API is one expected ");

        if (validID(Table.BOOK, nextIndexBook)) {

            boolean bookExists=validID(Table.BOOK, nextIndexBook);

            cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

            Assert.assertFalse(bookExists, "  POST-ing by mistake Book from API with missing  author(author=null) ");

        }
        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

    }

    @Test
    public static void testPostBookWithNullGenre() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        bookModel.setGenre(null);

        String postMessageReceived = PostAPI.post(bookModel);
//        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Missing genre\""), "check if  message received after POST-ing Book from API is one expected ");

        if (validID(Table.BOOK, nextIndexBook)) {

            boolean bookExists=validID(Table.BOOK, nextIndexBook);

            cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

            Assert.assertFalse(bookExists, "  POST-ing by mistake Book from API with missing  genre(genre=null) ");
        }

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
    }

    @Test
    public static void testPostBookWithNullID() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        String stringForInsert="null";

        String stringBook=transformBookStringForExecute(bookModel,"id",stringForInsert);
//        System.out.println(stringBook);
        String postMessageReceived = PostAPI.post(Table.BOOK,stringBook);
//        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Book from API is one expected ");

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
    }

    @Test
    public static void testPostBookWithNullName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        bookModel.setName(null);

        String postMessageReceived = PostAPI.post(bookModel);
//        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Missing book name\""), "check if  message received after POST-ing Book from API is one expected ");

        if (validID(Table.BOOK, nextIndexBook)) {

            boolean bookExists=validID(Table.BOOK, nextIndexBook);

            cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

            Assert.assertFalse(bookExists, "  POST-ing by mistake Book from API with missing  name(name=null) ");
        }

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

    }

    @Test
    public static void testPostBookWithNullPublicationDate() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        bookModel.setPublicationDate(null);

        String postMessageReceived = PostAPI.post(bookModel);
//        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Missing publication date\""), "check if  message received after POST-ing Book from API is one expected ");

        if (validID(Table.BOOK, nextIndexBook)) {

            boolean bookExists=validID(Table.BOOK, nextIndexBook);

            cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

            Assert.assertFalse(bookExists, "  POST-ing by mistake Book from API with missing  publication date(publication date =null) ");
        }

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
    }

    @Test
    public static void testPostBookWithEmptyID() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        String stringForInsert="\"\"";

        String stringBook=transformBookStringForExecute(bookModel,"id",stringForInsert);
//        System.out.println(stringBook);
        String postMessageReceived = PostAPI.post(Table.BOOK,stringBook);
//        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Book from API is one expected ");

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
    }

    @Test
    public static void testPostBookWithEmptyName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        bookModel.setName("");

        String postMessageReceived = PostAPI.post(bookModel);
//        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"The book name should have at least 1 character\""), "check if  message received after POST-ing Book from API is one expected ");

        if (validID(Table.BOOK, nextIndexBook)) {

            boolean bookExists=validID(Table.BOOK, nextIndexBook);

            cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

            Assert.assertFalse(bookExists, "  POST-ing by mistake Book from API with missing  name ");
        }

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
    }

    @Test
    public static void testPostBookWithEmptyPublicationDate() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        bookModel.setPublicationDate("");

        String postMessageReceived = PostAPI.post(bookModel);
//        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Missing publication date\""), "check if  message received after POST-ing Book from API is one expected ");

        if (validID(Table.BOOK, nextIndexBook)) {

            boolean bookExists = validID(Table.BOOK, nextIndexBook);

            cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

            Assert.assertFalse(bookExists, "  POST-ing by mistake Book from API with missing  publication date ");
        }

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
    }

    @Test
    public static void testPostBookWithLongerName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        bookModel.setName("more than 45 characters 12345678901234567890123456789012345678901234567890");

        String postMessageReceived = PostAPI.post(bookModel);
//        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"The book name MAX length is 45\""), "check if  message received after POST-ing Book from API is one expected ");

        if (validID(Table.BOOK, nextIndexBook)) {

            boolean bookExists=validID(Table.BOOK, nextIndexBook);

            cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

            Assert.assertFalse(bookExists, "  POST-ing by mistake Book from API with name longer than max allowed ");
        }

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
    }

    @Test
    public static void testPostBookWithLongerID() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        String stringForInsert="1234567890123456789";

        String stringBook=transformBookStringForExecute(bookModel,"id",stringForInsert);

        System.out.println(stringBook);


        String postMessageReceived = PostAPI.post(Table.BOOK,stringBook);
        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"JSON parse error: Numeric value ("+stringForInsert+") out of range of int"), "check if  message received after POST-ing Book from API is one expected ");

        if (validID(Table.BOOK, nextIndexBook)) {

            boolean bookExists=validID(Table.BOOK, nextIndexBook);

            cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

            Assert.assertFalse(bookExists, "  POST-ing by mistake Book from API with longer book ID >max   ");
        }

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);


    }

    @Test
    public static void testPostBookWithInvalidMonthPublicationDate() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        int month = getRandomNumber(22, 33);
        String publicationDateString = getRandomNumber(1970, 2019) + "-" + month + "-" + getRandomNumber(1, 27);


        bookModel.setPublicationDate(publicationDateString);

        String postMessageReceived = PostAPI.post(bookModel);
//        System.out.println("message" + postMessageReceived);

//        if (validID(Table.BOOK, nextIndexBook)) {
//
//            BookModel bookModelGetFromDB = (BookModel) (SelectDB.select(Table.BOOK, nextIndexBook).get(0));
//
//            boolean bookExists = validID(Table.BOOK, nextIndexBook);
//
//            cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
//
//            Assert.assertFalse(bookExists, "  POST-ing by mistake Book from API with wrong(invalid month='" + month + "')  publication date posted is='" + publicationDateString + "' and the one found in DB=" + bookModelGetFromDB.getPublicationDate());
//
//        }
//
//        Assert.assertFalse(postMessageReceived.contains("\"message\":\"Success!\""), "check if  message received after POST-ing Book from API is one expected ");

        throw new SkipException("Skipping for discussion");

//        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

    }

    @Test
    public static void testPostBookWithInvalidDayPublicationDate() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        int day=getRandomNumber(55,88);
        String publicationDateString=getRandomNumber(1970,2019)+"-"+getRandomNumber(1,12)+"-"+day;

        bookModel.setPublicationDate(publicationDateString);

        String postMessageReceived = PostAPI.post(bookModel);
//        System.out.println("message" + postMessageReceived);

//        if (validID(Table.BOOK, nextIndexBook)) {
//
//            BookModel bookModelGetFromDB = (BookModel)(SelectDB.select(Table.BOOK, nextIndexBook).get(0));
//
//            boolean bookExists=validID(Table.BOOK, nextIndexBook);
//
//            cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
//
//            Assert.assertFalse(bookExists, "  POST-ing by mistake Book from API with wrong(invalid day='"+day+"')  publication date posted='"+publicationDateString+"' and the one found in DB="+bookModelGetFromDB.getPublicationDate());
//
//        }
//
//        Assert.assertFalse(postMessageReceived.contains("\"message\":\"Success!\""), "check if  message received after POST-ing Book from API is one expected ");
//
//        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

        throw new SkipException("Skipping for discussion");


    }

    @Test
    public static void testPostBookWithInvalidIDAsString() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        String stringForInsert="abc"+getRandomText(6);

        String stringBook=transformBookStringForExecute(bookModel,"id",stringForInsert);
        System.out.println(stringBook);
        String postMessageReceived = PostAPI.post(Table.BOOK,stringBook);
        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"JSON parse error: Unrecognized token '"+stringForInsert+"'"), "check if  message received after POST-ing Book from API is one expected ");

        if (validID(Table.BOOK, nextIndexBook)) {

            boolean bookExists=validID(Table.BOOK, nextIndexBook);

            cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

            Assert.assertFalse(bookExists, "  POST-ing by mistake Book from API with invalid ID (post-ed as string   ");
        }

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
    }

    @Test
    public static void testPostBookWithInvalidIDAsStringWithBrakets() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        String stringForInsert="\""+getRandomText(6)+"\"";

        String stringBook=transformBookStringForExecute(bookModel,"id",stringForInsert);
        System.out.println(stringBook);
        String postMessageReceived = PostAPI.post(Table.BOOK,stringBook);
        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"JSON parse error: Can not deserialize value of type int from String"), "check if  message received after POST-ing Book from API is one expected ");

        if (validID(Table.BOOK, nextIndexBook)) {

            boolean bookExists=validID(Table.BOOK, nextIndexBook);

            cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

            Assert.assertFalse(bookExists, "  POST-ing by mistake Book from API with invalid ID (post-ed as string   ");
        }

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
    }

    @Test
    public static void testPostBookWithValidBookNameAsInteger() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        String stringForInsert=""+getRandomNumber(1,9999);

        String stringBook=transformBookStringForExecute(bookModel,"name",stringForInsert);
        System.out.println(stringBook);
        String postMessageReceived = PostAPI.post(Table.BOOK,stringBook);
        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Book from API is one expected ");


        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
    }

    @Test
    public static void testPostBookWithValidIDAsStringWithBrakets() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        String stringForInsert="\""+getRandomNumber(1,999999)+"\"";

        String stringBook=transformBookStringForExecute(bookModel,"id",stringForInsert);
        System.out.println(stringBook);
        String postMessageReceived = PostAPI.post(Table.BOOK,stringBook);
        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Book from API is one expected ");

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
    }

    @Test
    public static void testPostBookWithInvalidRandomPublicationDate() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        bookModel.setPublicationDate(getRandomText(9));

        String postMessageReceived = PostAPI.post(bookModel);
        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"JSON parse error: Can not deserialize value of type java.util.Date from String "), "check if  message received after POST-ing Book from API is one expected ");

        if (validID(Table.BOOK, nextIndexBook)) {

            boolean bookExists = validID(Table.BOOK, nextIndexBook);

            cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

            Assert.assertFalse(bookExists, "  POST-ing by mistake Book from API with wrong  publication date ");
        }

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
    }

    @Test
    public static void testPostBookWithValidPublicationDateWithNoBrakets() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel = prepareBookForPost();

        String stringForInsert=""+getRandomNumber(1970,2019)+"-"+getRandomNumber(1,12)+"-"+getRandomNumber(1,27);

        String stringBook=transformBookStringForExecute(bookModel,"publicationDate",stringForInsert);
        System.out.println(stringBook);
        String postMessageReceived = PostAPI.post(Table.BOOK,stringBook);
        System.out.println("message" + postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"JSON parse error:"), "check if  message received after POST-ing Book from API is one expected ");

        if (validID(Table.BOOK, nextIndexBook)) {

            boolean bookExists = validID(Table.BOOK, nextIndexBook);

            cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);

            Assert.assertFalse(bookExists, "  POST-ing by mistake Book from API with wrong  publication date in post ");
        }

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
    }























    @Test
    public static void testPostGenre() throws IOException {

//        GenreModel genreModel = getRandomGenreModel(8);

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        int countBeforePost = getDBCount(Table.GENRE);

//        genreModel.setId(nextIndexGenre);

        GenreModel genreModel=prepareGenreForPost();


        String postMessageReceived = PostAPI.post(genreModel);

        int countAfterPost = getDBCount(Table.GENRE);//always after post method//

        GenreModel genreModel1GetFromDB = (GenreModel)(SelectDB.select(Table.GENRE, nextIndexGenre).get(0));


        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Genre from API is successful ");

        Assert.assertEquals(countAfterPost, countBeforePost + 1, "check if DB number of " + Table.GENRE + " records after POST is 1 greater than before POST ");

        Assert.assertTrue(modelsHaveEqualValues(genreModel, genreModel1GetFromDB), "check if the random data model POST-ed through API is also  find in DB");

//        System.out.println("message" + postMessageReceived);
//
//        printAllDataModels(genreModel);
//        printAllDataModels(SelectDB.select(Table.GENRE, nextIndexGenre));

        cleanModel(Table.GENRE, nextIndexGenre);

    }

    @Test
    public static void testPostGenreWithExistentID() throws IOException {

        int indexToBeImposed=getNextAutoIncrement(Table.GENRE);

        GenreModel genreModel = prepareGenreForPost();

        InsertDB.insert(genreModel);

        GenreModel genreModelForPost=getRandomGenreModel(9);

        genreModelForPost.setId(indexToBeImposed);

        int indexToBePosted=getNextAutoIncrement(Table.GENRE);


        String postMessageReceived = PostAPI.post(genreModelForPost);

        GenreModel genreModelGetFromDB = (GenreModel) (SelectDB.select(Table.GENRE, indexToBePosted).get(0));


        genreModelForPost.setId(indexToBeImposed+1);

        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Genre from API is successful ");

        Assert.assertTrue(modelsHaveEqualValues(genreModelForPost, genreModelGetFromDB), "check if the random data model POST-ed through API is also  find in DB");

        cleanModel(Table.GENRE, indexToBeImposed);
        cleanModel(Table.GENRE, indexToBePosted);

    }

    @Test
    public static void testPostGenreWithNullName() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        GenreModel genreModel=prepareGenreForPost();

        genreModel.setName(null);

        String postMessageReceived = PostAPI.post(genreModel);

       System.out.println(postMessageReceived);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Missing genre name\""), "check if  message received after POST-ing Genre from API is the one expected ");

        // clean from DB the 'AUTHOR' if is POST-ed by mistake by the test and fail the assert test
        if (validID(Table.GENRE, nextIndexGenre)) {

            boolean genreExists=validID(Table.GENRE, nextIndexGenre);

            cleanModel(Table.GENRE, nextIndexGenre);

            Assert.assertFalse(genreExists, "  POST-ing by mistake Genre from API with missing  Name(name = null) ");

        }

    }

    @Test
    public static void testPostGenreWithNullID() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        GenreModel genreModel=prepareGenreForPost();

        String stringForInsert="null";

        String stringGenre=transformGenreStringForExecute(genreModel,"id",stringForInsert);
//        System.out.println(stringGenre);
        String postMessageReceived = PostAPI.post(Table.GENRE,stringGenre);
//        System.out.println(postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Genre from API is the one expected ");

        cleanModel(Table.GENRE, nextIndexGenre);

    }

    @Test
    public static void testPostGenreWithEmptyName() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        GenreModel genreModel=prepareGenreForPost();

        genreModel.setName("");

        String postMessageReceived = PostAPI.post(genreModel);

        System.out.println(postMessageReceived);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"The genre name should have at least 1 character\""), "check if  message received after POST-ing Genre from API is the one expected ");

        // clean from DB the 'AUTHOR' if is POST-ed by mistake by the test and fail the assert test
        if (validID(Table.GENRE, nextIndexGenre)) {

            boolean genreExists=validID(Table.GENRE, nextIndexGenre);

            cleanModel(Table.GENRE, nextIndexGenre);

            Assert.assertFalse(genreExists, "  POST-ing by mistake Genre from API with missing  Name ");

        }

    }

    @Test
        public static void testPostGenreWithLongerName() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        GenreModel genreModel=prepareGenreForPost();

        genreModel.setName("more than 45 characters 12345678901234567890123456789012345678901234567890");

        String postMessageReceived = PostAPI.post(genreModel);

        System.out.println(postMessageReceived);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"The genre name MAX length is 45\""), "check if  message received after POST-ing Genre from API is the one expected ");

        // clean from DB the 'genre' if is POST-ed by mistake by the test and fail the assert test
        if (validID(Table.GENRE, nextIndexGenre)) {

            boolean genreExists=validID(Table.GENRE, nextIndexGenre);

            cleanModel(Table.GENRE, nextIndexGenre);

            Assert.assertFalse(genreExists, "  POST-ing by mistake Genre from API with  Name longer than max ");

        }

    }

    @Test
    public static void testPostGenreWithLongerID() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        GenreModel genreModel=prepareGenreForPost();

        String stringForInsert="123456789012345";

        String stringGenre=transformGenreStringForExecute(genreModel,"id",stringForInsert);
//        System.out.println(stringGenre);
        String postMessageReceived = PostAPI.post(Table.GENRE,stringGenre);
//        System.out.println(postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"JSON parse error: Numeric value ("+stringForInsert+") out of range of int"), "check if  message received after POST-ing Genre from API is the one expected ");

//         clean from DB the 'genre' if is POST-ed by mistake by the test and fail the assert test
        if (validID(Table.GENRE, nextIndexGenre)) {

            boolean genreExists=validID(Table.GENRE, nextIndexGenre);

            cleanModel(Table.GENRE, nextIndexGenre);

            Assert.assertFalse(genreExists, "  POST-ing by mistake Genre from API with  ID longer than max ");
        }

    }

    @Test
    public static void testPostGenreTwice() throws IOException {


        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        GenreModel genreModel=prepareGenreForPost();

        InsertDB.insert(genreModel);

        String postMessageReceived = PostAPI.post(genreModel);


        System.out.println("message" + postMessageReceived);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Genre already exists\""), "check if  message received after trying to POST again the same Author from API is the one expected ");

        cleanModel(Table.GENRE,nextIndexGenre);

    }

    @Test
    public static void testPostGenreWithIDAsStringWithNoNumber() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        GenreModel genreModel=prepareGenreForPost();

        String stringForInsert="abc"+getRandomText(5);

        String stringGenre=transformGenreStringForExecute(genreModel,"id",stringForInsert);
        System.out.println(stringGenre);
        String postMessageReceived = PostAPI.post(Table.GENRE,stringGenre);
        System.out.println(postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"JSON parse error: Unrecognized token '"+stringForInsert+"'"), "check if  message received after POST-ing Genre from API is the one expected ");

//         clean from DB the 'genre' if is POST-ed by mistake by the test and fail the assert test
        if (validID(Table.GENRE, nextIndexGenre)) {

            boolean genreExists=validID(Table.GENRE, nextIndexGenre);

            cleanModel(Table.GENRE, nextIndexGenre);

            Assert.assertFalse(genreExists, "  POST-ing by mistake Genre from API with  ID insert as String");
        }

        cleanModel(Table.GENRE, nextIndexGenre);


    }

    @Test
    public static void testPostGenreWithIDAsStringAsInteger() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        GenreModel genreModel=prepareGenreForPost();

        String stringForInsert="\""+getRandomText(9)+"\"";

        String stringGenre=transformGenreStringForExecute(genreModel,"id",stringForInsert);
        System.out.println(stringGenre);
        String postMessageReceived = PostAPI.post(Table.GENRE,stringGenre);
        System.out.println(postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("\"message\":\"JSON parse error: Can not deserialize value of type int from String"), "check if  message received after POST-ing Genre from API is the one expected ");

        if (validID(Table.GENRE, nextIndexGenre)) {

            boolean genreExists=validID(Table.GENRE, nextIndexGenre);

            cleanModel(Table.GENRE, nextIndexGenre);

            Assert.assertFalse(genreExists, "  POST-ing by mistake Genre from API with  ID insert as String");
        }

        cleanModel(Table.GENRE,nextIndexGenre);

    }

    @Test
    public static void testPostGenreWithNameAsInteger() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        GenreModel genreModel=prepareGenreForPost();

        String stringForInsert=""+getRandomNumber(1,999999);

        String stringGenre=transformGenreStringForExecute(genreModel,"name",stringForInsert);
        System.out.println(stringGenre);
        String postMessageReceived = PostAPI.post(Table.GENRE,stringGenre);
        System.out.println(postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Genre from API is the one expected ");

        cleanModel(Table.GENRE,nextIndexGenre);

    }

    @Test
    public static void testPostGenreWithIDAsIntegerString() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        GenreModel genreModel=prepareGenreForPost();

        String stringForInsert="\""+getRandomNumber(1,999999)+"\"";

        String stringGenre=transformGenreStringForExecute(genreModel,"id",stringForInsert);
        System.out.println(stringGenre);
        String postMessageReceived = PostAPI.post(Table.GENRE,stringGenre);
        System.out.println(postMessageReceived);
        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing Genre from API is the one expected ");

        cleanModel(Table.GENRE,nextIndexGenre);

    }






    @Test
    public static void testPostUser() throws IOException {

        int nextIndexUser = getNextAutoIncrement(Table.USER);

        int countBeforePost = getDBCount(Table.USER);

        UserModel userModel=prepareUserForPost();
//            printOneDataModels(userModel);
        String postMessageReceived = PostAPI.post(userModel);
//             System.out.println("message" + postMessageReceived);
        int countAfterPost = getDBCount(Table.USER);//always after post method//

        UserModel userModel1GetFromDB = (UserModel) (SelectDB.select(Table.USER, nextIndexUser).get(0));
//             printOneDataModels(userModel1GetFromDB);
        Assert.assertTrue(postMessageReceived.contains("Success"), "check if  message received after POST-ing User from API is successful ");

        Assert.assertEquals(countAfterPost, countBeforePost + 1, "check if DB number of " + Table.USER + " records after POST is 1 greater than before POST ");

        Assert.assertTrue(modelsHaveEqualValues(userModel, userModel1GetFromDB), "check if the random data model POST-ed through API is also  find in DB");

        cleanModel(Table.USER, nextIndexUser);
    }

    @Test
    public static void testPostUserTwice() throws IOException {


        int nextIndexUser = getNextAutoIncrement(Table.USER);

        UserModel userModel=prepareUserForPost();

        InsertDB.insert(userModel);
//        printOneDataModels(userModel);
        UserModel userModel1ForPost=userModel;

        int countBeforePost = getDBCount(Table.USER);

        String postMessageReceived = PostAPI.post(userModel1ForPost);
//        System.out.println("message" + postMessageReceived);
        int countAfterPost = getDBCount(Table.USER);//always after post method//

        cleanModel(Table.USER, nextIndexUser);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Username already exists\""), "check if  message received after POST-ing User from API is successful ");
        Assert.assertEquals(countAfterPost, countBeforePost , "check if DB number of " + Table.USER + " records after POST is same than before POST");

    }

    @Test
    public static void testPostUserWithNullUserName() throws IOException {

        int nextIndexUser = getNextAutoIncrement(Table.USER);

        int countBeforePost = getDBCount(Table.USER);

        UserModel userModel=prepareUserForPost();
//            printOneDataModels(userModel);
            userModel.setUsername(null);
        String postMessageReceived = PostAPI.post(userModel);
//             System.out.println("message" + postMessageReceived);
        int countAfterPost = getDBCount(Table.USER);//always after post method//

        cleanModel(Table.USER, nextIndexUser);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Missing username\""), "check if  message received after POST-ing User from API is the one expected ");

        Assert.assertEquals(countAfterPost, countBeforePost , "check if DB number of " + Table.USER + " records after POST is the same than before POST ");

    }

    @Test
    public static void testPostUserWithNullPassword() throws IOException {

        int nextIndexUser = getNextAutoIncrement(Table.USER);

        int countBeforePost = getDBCount(Table.USER);

        UserModel userModel=prepareUserForPost();
//        printOneDataModels(userModel);
        userModel.setPassword(null);
        String postMessageReceived = PostAPI.post(userModel);
//        System.out.println("message" + postMessageReceived);
        int countAfterPost = getDBCount(Table.USER);//always after post method//

        cleanModel(Table.USER, nextIndexUser);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"Missing password\""), "check if  message received after POST-ing User from API is the one expected ");

        Assert.assertEquals(countAfterPost, countBeforePost , "check if DB number of " + Table.USER + " records after POST is the same than before POST ");

    }

    @Test
    public static void testPostUserWithEmptyUserName() throws IOException {

        int nextIndexUser = getNextAutoIncrement(Table.USER);

        int countBeforePost = getDBCount(Table.USER);

        UserModel userModel=prepareUserForPost();
//        printOneDataModels(userModel);
        userModel.setUsername("");
        String postMessageReceived = PostAPI.post(userModel);
//        System.out.println("message" + postMessageReceived);
        int countAfterPost = getDBCount(Table.USER);//always after post method//

        cleanModel(Table.USER, nextIndexUser);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"The username should have at least 3 characters\""), "check if  message received after POST-ing User from API is the one expected ");

        Assert.assertEquals(countAfterPost, countBeforePost , "check if DB number of " + Table.USER + " records after POST is the same than before POST ");

    }

    @Test
    public static void testPostUserWithEmptyPassword() throws IOException {

        int nextIndexUser = getNextAutoIncrement(Table.USER);

        int countBeforePost = getDBCount(Table.USER);

        UserModel userModel=prepareUserForPost();
//        printOneDataModels(userModel);
        userModel.setPassword("");
        String postMessageReceived = PostAPI.post(userModel);
//        System.out.println("message" + postMessageReceived);
        int countAfterPost = getDBCount(Table.USER);//always after post method//

        cleanModel(Table.USER, nextIndexUser);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"The password should have at least 8 characters\""), "check if  message received after POST-ing User from API is the one expected ");

        Assert.assertEquals(countAfterPost, countBeforePost , "check if DB number of " + Table.USER + " records after POST is the same than before POST ");

    }

    @Test
    public static void testPostUserWithLongerUserName() throws IOException {

        int nextIndexUser = getNextAutoIncrement(Table.USER);

        int countBeforePost = getDBCount(Table.USER);

        UserModel userModel=prepareUserForPost();
//        printOneDataModels(userModel);
        userModel.setUsername("more than 45 char"+getRandomText(46));
        String postMessageReceived = PostAPI.post(userModel);
        System.out.println("message" + postMessageReceived);
        int countAfterPost = getDBCount(Table.USER);//always after post method//

        cleanModel(Table.USER, nextIndexUser);

        Assert.assertTrue(postMessageReceived.contains("\"message\":\"The usernamename MAX length is 45 characters\""), "check if  message received after POST-ing User from API is the one expected ");

        Assert.assertEquals(countAfterPost, countBeforePost , "check if DB number of " + Table.USER + " records after POST is the same than before POST ");

    }

    @Test
    public static void testPostUserWithLongerPassword() throws IOException {

        int nextIndexUser = getNextAutoIncrement(Table.USER);

        int countBeforePost = getDBCount(Table.USER);

        UserModel userModel=prepareUserForPost();
//        printOneDataModels(userModel);
        userModel.setPassword("more than 300 char"+getRandomText(301));
        String postMessageReceived = PostAPI.post(userModel);
        System.out.println("message" + postMessageReceived);
        int countAfterPost = getDBCount(Table.USER);//always after post method//

        cleanModel(Table.USER, nextIndexUser);

        throw new SkipException("Skipping for discussion");

//        Assert.assertFalse(postMessageReceived.contains("success"), "check if  message received after POST-ing User from API is not successful ");
//
//        Assert.assertEquals(countAfterPost, countBeforePost , "check if DB number of " + Table.USER + " records after POST is the same than before POST (password length>300) ");

    }




}

