package ro.siit.tau.bookslib.tests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.Test;
import ro.siit.tau.bookslib.models.AuthorModel;
import ro.siit.tau.bookslib.models.BookModel;
import ro.siit.tau.bookslib.models.GenreModel;
import ro.siit.tau.bookslib.services.*;

import java.io.IOException;
import java.util.List;

import static ro.siit.tau.bookslib.utils.DBUtil.*;

public class TestGetAPI extends BaseTest {



    private static boolean identicalListOfModel(List<Object> firstList,List<Object> secondList){

        if (firstList.size() != secondList.size()) {
            System.out.println("lists for compare have different sizes");
            return false;
        }

        if ((firstList.size() == 0) || (secondList.size() == 0)) {
            System.out.println("one of the list is empty");
            return false;
        }

        if ((firstList.get(0) instanceof AuthorModel)&(secondList.get(0) instanceof AuthorModel)) {

            for (int i = 0; i < firstList.size(); i++) {
                if (!modelsHaveEqualValues((AuthorModel)(firstList.get(i)), (AuthorModel)(getModelWithCertainIDFromList(secondList,((AuthorModel)(firstList.get(i))).getId())))){return false;}
            }
        }

        if ((firstList.get(0) instanceof BookModel)&(secondList.get(0) instanceof BookModel)) {

            for (int i = 0; i < firstList.size(); i++) {
                if (!modelsHaveEqualValues((BookModel)(firstList.get(i)), (BookModel)(getModelWithCertainIDFromList(secondList,((BookModel)(firstList.get(i))).getId())))){return false;}
            }
        }

        if ((firstList.get(0) instanceof GenreModel)&(secondList.get(0) instanceof GenreModel)) {

            for (int i = 0; i < firstList.size(); i++) {
                if (!modelsHaveEqualValues((GenreModel)(firstList.get(i)), (GenreModel)(getModelWithCertainIDFromList(secondList,((GenreModel)(firstList.get(i))).getId())))){return false;}
            }
        }
        return true;
    }

    private static Object getModelWithCertainIDFromList(List<Object> list,int id){


        if (list.get(0) instanceof AuthorModel) {

            for (int i = 0; i < list.size(); i++) {
                if( ( (AuthorModel)(list.get(i)) ).getId()==id ){return list.get(i); }
            }
        }

        if (list.get(0) instanceof BookModel) {

            for (int i = 0; i < list.size(); i++) {
                if( ( (BookModel)(list.get(i)) ).getId()==id ){return list.get(i); }
            }
        }

        if (list.get(0) instanceof GenreModel) {

            for (int i = 0; i < list.size(); i++) {
                if( ( (GenreModel)(list.get(i)) ).getId()==id ){return list.get(i); }
            }
        }
        return null;
    }

    private static BookModel prepareBookForGet(){

        AuthorModel authorModel = getRandomAuthorModel(7);
        GenreModel genreModel = getRandomGenreModel(5);
        BookModel bookModel = getRandomBookModel(6);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
//        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        InsertDB.insert(authorModel);
        InsertDB.insert(genreModel);

        bookModel.getAuthor().setId(nextIndexAuthor);
        bookModel.getGenre().setId(nextIndexGenre);

        InsertDB.insert(bookModel);

        return bookModel;
    }

    private static BookModel prepareBookForGetWithoutInsertInDB(){

        AuthorModel authorModel = getRandomAuthorModel(7);
        GenreModel genreModel = getRandomGenreModel(5);
        BookModel bookModel = getRandomBookModel(6);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
//        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        InsertDB.insert(authorModel);
        InsertDB.insert(genreModel);

        bookModel.getAuthor().setId(nextIndexAuthor);
        bookModel.getGenre().setId(nextIndexGenre);

        return bookModel;
    }


    @Test
    public static void testGetAllAuthorFromAPI() throws IOException {

        // in case DB has no author
        boolean shouldDeleteAfterTest=false;
        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);


        if(getDBCount(Table.AUTHOR)==0){
             shouldDeleteAfterTest=true;

             AuthorModel authorModel = getRandomAuthorModel(6);

            InsertDB.insert(authorModel);
        }

        String responseBody=GetAPI.get(Table.AUTHOR);

//        System.out.println(responseBody);

        ObjectMapper mapper = new ObjectMapper();
        List<Object> authorListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<AuthorModel>>() {});


        List<Object> authorListFromDB= SelectDB.select(Table.AUTHOR);

//        printAllDataModels(authorListFromAPIResponse);
//        System.out.println("DB:");
//        printAllDataModels(authorListFromDB);
        if(shouldDeleteAfterTest){
            cleanModel(Table.AUTHOR,nextIndexAuthor);
        }

        Assert.assertEquals(authorListFromAPIResponse.size(),authorListFromDB.size(),"check for matching  list size get from API and DB");
        Assert.assertTrue(identicalListOfModel(authorListFromAPIResponse,authorListFromDB),"check for matching  lists  get from API and DB");

    }

    @Test
    public static void testGetSpecificAuthorFromAPI() throws IOException {

        AuthorModel authorModel = getRandomAuthorModel(6);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        InsertDB.insert(authorModel);

        String responseBody=GetAPI.get(Table.AUTHOR,nextIndexAuthor);

        responseBody="["+responseBody+"]";

//        System.out.println(responseBody);

        ObjectMapper mapper = new ObjectMapper();
        List<Object> authorListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<AuthorModel>>() {});



        List<Object> authorListFromDB= SelectDB.select(Table.AUTHOR,nextIndexAuthor);


//        printAllDataModels(authorListFromAPIResponse);
//        System.out.println("DB:");
//        printAllDataModels(authorListFromDB);

        cleanModel(Table.AUTHOR,nextIndexAuthor);

        Assert.assertEquals(authorListFromAPIResponse.size(),authorListFromDB.size(),"check for matching  list size get from API and DB");
        Assert.assertTrue(identicalListOfModel(authorListFromAPIResponse,authorListFromDB),"check for matching  lists  get from API and DB");

        //clean DB
//        if (validID(Table.AUTHOR, nextIndexAuthor)) {
//            DeleteDB.delete(Table.AUTHOR, nextIndexAuthor);
//        }

        // System.out.println(authorList.get(0).getClass());

        //System.out.println(authorList.size());
    }

    @Test
    public static void testGetSpecificAuthorWithInvalidID() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        String responseBody=GetAPI.get(Table.AUTHOR,nextIndexAuthor);
//        System.out.println(responseBody);
        Assert.assertTrue(responseBody.contains("\"message\":\"Author with id "+nextIndexAuthor+" was not found\""),"check check if response message is the one  expected ");
    }

    @Test
    public static void testGetAllAuthorWhereOneIsWithNullFirstName() throws IOException {

        AuthorModel authorModel = getRandomAuthorModel(6);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        authorModel.setFirstName(null);

        InsertDB.insert(authorModel);

        String responseBody=GetAPI.get(Table.AUTHOR);
//       System.out.println(responseBody);
        ObjectMapper mapper = new ObjectMapper();
        List<Object> authorListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<AuthorModel>>() {});
//        printAllDataModels(authorListFromAPIResponse);
        List<Object> authorListFromDB= SelectDB.select(Table.AUTHOR);
//        printAllDataModels(authorListFromDB);
        cleanModel(Table.AUTHOR,nextIndexAuthor);

        Assert.assertEquals(authorListFromAPIResponse.size(),authorListFromDB.size(),"check for matching  list size get from API and DB");
        Assert.assertTrue(identicalListOfModel(authorListFromAPIResponse,authorListFromDB),"check for matching  lists  get from API and DB");

    }




    @Test
    public static void testGetSpecificAuthorWithNullFirstName() throws IOException {

        AuthorModel authorModel = getRandomAuthorModel(6);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        authorModel.setFirstName(null);

        InsertDB.insert(authorModel);


        String responseBody=GetAPI.get(Table.AUTHOR,nextIndexAuthor);

        responseBody="["+responseBody+"]";
//        System.out.println(responseBody);
        ObjectMapper mapper = new ObjectMapper();
        List<Object> authorListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<AuthorModel>>() {});


        List<Object> authorListFromDB= SelectDB.select(Table.AUTHOR,nextIndexAuthor);

         cleanModel(Table.AUTHOR,nextIndexAuthor);

        Assert.assertEquals(authorListFromAPIResponse.size(),authorListFromDB.size(),"check for matching  list size get from API and DB");
        Assert.assertTrue(identicalListOfModel(authorListFromAPIResponse,authorListFromDB),"check for matching  lists  get from API and DB");

    }

    @Test
    public static void testGetAllAuthorWhereOneIsWithNullLastName() throws IOException {

        AuthorModel authorModel = getRandomAuthorModel(6);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        authorModel.setLastName(null);

        InsertDB.insert(authorModel);

        String responseBody=GetAPI.get(Table.AUTHOR);
//       System.out.println(responseBody);
        ObjectMapper mapper = new ObjectMapper();
        List<Object> authorListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<AuthorModel>>() {});
//        printAllDataModels(authorListFromAPIResponse);
        List<Object> authorListFromDB= SelectDB.select(Table.AUTHOR);
//        printAllDataModels(authorListFromDB);
        cleanModel(Table.AUTHOR,nextIndexAuthor);

        Assert.assertEquals(authorListFromAPIResponse.size(),authorListFromDB.size(),"check for matching  list size get from API and DB");
        Assert.assertTrue(identicalListOfModel(authorListFromAPIResponse,authorListFromDB),"check for matching  lists  get from API and DB");

    }

    @Test
    public static void testGetSpecificAuthorWithLastName() throws IOException {

        AuthorModel authorModel = getRandomAuthorModel(6);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        authorModel.setLastName(null);

        InsertDB.insert(authorModel);


        String responseBody=GetAPI.get(Table.AUTHOR,nextIndexAuthor);

        responseBody="["+responseBody+"]";

//        System.out.println(responseBody);

        ObjectMapper mapper = new ObjectMapper();
        List<Object> authorListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<AuthorModel>>() {});


        List<Object> authorListFromDB= SelectDB.select(Table.AUTHOR,nextIndexAuthor);

         cleanModel(Table.AUTHOR,nextIndexAuthor);

        Assert.assertEquals(authorListFromAPIResponse.size(),authorListFromDB.size(),"check for matching  list size get from API and DB");
        Assert.assertTrue(identicalListOfModel(authorListFromAPIResponse,authorListFromDB),"check for matching  lists  get from API and DB");

    }

    @Test
    public static void testGetAllAuthorWhereOneIsWithEmptyFirstName() throws IOException {

        AuthorModel authorModel = getRandomAuthorModel(6);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        authorModel.setFirstName("");

        InsertDB.insert(authorModel);

        String responseBody=GetAPI.get(Table.AUTHOR);
       System.out.println(responseBody);
        ObjectMapper mapper = new ObjectMapper();
        List<Object> authorListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<AuthorModel>>() {});
//        printAllDataModels(authorListFromAPIResponse);
        List<Object> authorListFromDB= SelectDB.select(Table.AUTHOR);
//        printAllDataModels(authorListFromDB);
        cleanModel(Table.AUTHOR,nextIndexAuthor);

        Assert.assertEquals(authorListFromAPIResponse.size(),authorListFromDB.size(),"check for matching  list size get from API and DB");
        Assert.assertTrue(identicalListOfModel(authorListFromAPIResponse,authorListFromDB),"check for matching  lists  get from API and DB");

    }



    @Test
    public static void testGetSpecificAuthorWithEmptyFirstName() throws IOException {

        AuthorModel authorModel = getRandomAuthorModel(6);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        authorModel.setFirstName("");

        InsertDB.insert(authorModel);


        String responseBody=GetAPI.get(Table.AUTHOR,nextIndexAuthor);

        responseBody="["+responseBody+"]";
        System.out.println(responseBody);
        ObjectMapper mapper = new ObjectMapper();
        List<Object> authorListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<AuthorModel>>() {});


        List<Object> authorListFromDB= SelectDB.select(Table.AUTHOR,nextIndexAuthor);

        cleanModel(Table.AUTHOR,nextIndexAuthor);

        Assert.assertEquals(authorListFromAPIResponse.size(),authorListFromDB.size(),"check for matching  list size get from API and DB");
        Assert.assertTrue(identicalListOfModel(authorListFromAPIResponse,authorListFromDB),"check for matching  lists  get from API and DB");

    }










    @Test
    public static void testGetAllBookFromAPI() throws IOException {
        // in case DB has no book
        boolean shouldDeleteAfterTest=false;

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        if (getDBCount(Table.BOOK) == 0) {
            shouldDeleteAfterTest = true;

            prepareBookForGet();
        }

        String responseBody=GetAPI.get(Table.BOOK);
//        System.out.println(responseBody);
        ObjectMapper mapper = new ObjectMapper();
        List<Object> bookListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<BookModel>>() {});

        List<Object> bookListFromDB= SelectDB.select(Table.BOOK);

        if(shouldDeleteAfterTest){
            cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);
        }

        Assert.assertEquals(bookListFromAPIResponse.size(),bookListFromDB.size(),"check for matching  list size get from API and DB");
        Assert.assertTrue(identicalListOfModel(bookListFromAPIResponse,bookListFromDB),"check for matching  lists  get from API and DB");

    }

    @Test
    public static void testGetSpecificBookFromAPI() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        prepareBookForGet();

        String responseBody=GetAPI.get(Table.BOOK,nextIndexBook);

        responseBody="["+responseBody+"]";

        System.out.println(responseBody);

        ObjectMapper mapper = new ObjectMapper();

        List<Object> bookListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<BookModel>>() {});

        List<Object> bookListFromDB= SelectDB.select(Table.BOOK,nextIndexBook);

        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);

        Assert.assertEquals(bookListFromAPIResponse.size(),bookListFromDB.size(),"check for matching  list size(should be 1) get from API and DB");
        Assert.assertTrue(identicalListOfModel(bookListFromAPIResponse,bookListFromDB),"check for matching  book  get from API and DB");

    }

    @Test
    public static void testGetSpecificBookWithInvalidID() throws IOException {

        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        String responseBody=GetAPI.get(Table.BOOK, nextIndexBook);

        System.out.println(responseBody);

        Assert.assertTrue(responseBody.contains("\"message\":\"Book with id "+nextIndexBook+" was not found\""),"check if response message is the one  expected");

    }

    @Test
    public static void testGetSpecificBookWithNullName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel=prepareBookForGetWithoutInsertInDB();

        bookModel.setName(null);

        InsertDB.insert(bookModel);

        String responseBody=GetAPI.get(Table.BOOK,nextIndexBook);

        responseBody="["+responseBody+"]";

        System.out.println(responseBody);

        ObjectMapper mapper = new ObjectMapper();

        List<Object> bookListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<BookModel>>() {});

        List<Object> bookListFromDB= SelectDB.select(Table.BOOK,nextIndexBook);

        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);

        Assert.assertEquals(bookListFromAPIResponse.size(),bookListFromDB.size(),"check for matching  list size(should be 1) get from API and DB");
        Assert.assertTrue(identicalListOfModel(bookListFromAPIResponse,bookListFromDB),"check for matching  book  get from API and DB");

    }

    @Test
    public static void testGetAllBooksWhereOneIsWithNullName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel=prepareBookForGetWithoutInsertInDB();

        bookModel.setName(null);

        InsertDB.insert(bookModel);

        String responseBody=GetAPI.get(Table.BOOK);

//        responseBody="["+responseBody+"]";

        System.out.println(responseBody);

        ObjectMapper mapper = new ObjectMapper();

        List<Object> bookListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<BookModel>>() {});

        List<Object> bookListFromDB= SelectDB.select(Table.BOOK);

        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);

        Assert.assertEquals(bookListFromAPIResponse.size(),bookListFromDB.size(),"check for matching  list size(should be 1) get from API and DB");
        Assert.assertTrue(identicalListOfModel(bookListFromAPIResponse,bookListFromDB),"check for matching  book  get from API and DB");

    }

    @Test
    public static void testGetSpecificBookWithEmptyName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel=prepareBookForGetWithoutInsertInDB();

        bookModel.setName("");

        InsertDB.insert(bookModel);

        String responseBody=GetAPI.get(Table.BOOK,nextIndexBook);

        responseBody="["+responseBody+"]";

        System.out.println(responseBody);

        ObjectMapper mapper = new ObjectMapper();

        List<Object> bookListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<BookModel>>() {});

        List<Object> bookListFromDB= SelectDB.select(Table.BOOK,nextIndexBook);

        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);

        Assert.assertEquals(bookListFromAPIResponse.size(),bookListFromDB.size(),"check for matching  list size(should be 1) get from API and DB");
        Assert.assertTrue(identicalListOfModel(bookListFromAPIResponse,bookListFromDB),"check for matching  book  get from API and DB");

    }

    @Test
    public static void testGetAllBooksWhereOneIsWithEmptyName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModel=prepareBookForGetWithoutInsertInDB();

        bookModel.setName("");

        InsertDB.insert(bookModel);

        String responseBody=GetAPI.get(Table.BOOK);

        System.out.println(responseBody);

        ObjectMapper mapper = new ObjectMapper();

        List<Object> bookListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<BookModel>>() {});

        List<Object> bookListFromDB= SelectDB.select(Table.BOOK);

        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);

        Assert.assertEquals(bookListFromAPIResponse.size(),bookListFromDB.size(),"check for matching  list size(should be 1) get from API and DB");
        Assert.assertTrue(identicalListOfModel(bookListFromAPIResponse,bookListFromDB),"check for matching  book  get from API and DB");

    }












    @Test
    public static void testGetAllGenreFromAPI() throws IOException {
        // in case DB has no genre
        boolean shouldDeleteAfterTest=false;
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        if(getDBCount(Table.GENRE)==0){
            shouldDeleteAfterTest=true;

            GenreModel genreModel = getRandomGenreModel(6);

            InsertDB.insert(genreModel);
        }

        String responseBody=GetAPI.get(Table.GENRE);

//       System.out.println(responseBody);

        ObjectMapper mapper = new ObjectMapper();
        List<Object> genreListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<GenreModel>>() {});

        List<Object> genreListFromDB= SelectDB.select(Table.GENRE);

//        printAllDataModels(genreListFromAPIResponse);
//        System.out.println("DB:");
//        printAllDataModels(genreListFromDB);

        if(shouldDeleteAfterTest){
            cleanModel(Table.GENRE,nextIndexGenre);
        }

        Assert.assertEquals(genreListFromAPIResponse.size(),genreListFromDB.size(),"check for matching  list size get from API and DB");
        Assert.assertTrue(identicalListOfModel(genreListFromAPIResponse,genreListFromDB),"check for matching  lists  get from API and DB");

    }

    @Test
    public static void testGetSpecificGenreFromAPI() throws IOException {

        GenreModel genreModel = getRandomGenreModel(6);

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        InsertDB.insert(genreModel);

        String responseBody=GetAPI.get(Table.GENRE,nextIndexGenre);

        responseBody="["+responseBody+"]";
//       System.out.println(responseBody);
        ObjectMapper mapper = new ObjectMapper();

        List<Object> genreListFromAPIResponse = mapper.readValue(responseBody,
                new TypeReference<List<GenreModel>>() {});

        List<Object> genreListFromDB= SelectDB.select(Table.GENRE,nextIndexGenre);

        cleanModel(Table.GENRE,nextIndexGenre);

        Assert.assertEquals(genreListFromAPIResponse.size(),genreListFromDB.size(),"check for matching  list size(should be 1) get from API and DB");
        Assert.assertTrue(identicalListOfModel(genreListFromAPIResponse,genreListFromDB),"check for matching  genre  get from API and DB");

    }

    @Test
    public static void testGetSpecificGenreWithInvalidID() throws IOException {

        String responseBody=GetAPI.get(Table.GENRE,getNextAutoIncrement(Table.GENRE));
//       System.out.println(responseBody);
        Assert.assertTrue(responseBody.contains("\"message\":\"Genre was not found\""),"check if response message is the one  expected");
    }

//    @Test
//    public static void testGetSpecificGenreWhereNameIsEmpty() throws IOException {

//        GenreModel genreModel = getRandomGenreModel(6);
//
//        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
//
//        genreModel.setName("");
//
//        List<Object> initialGenreListFromDB= SelectDB.select(Table.GENRE,nextIndexGenre);
//
//
//        boolean isNameAlreadyUsed=false;
//
//        for (int i = 0; i <initialGenreListFromDB.size() ; i++) {
//            if( ((GenreModel)initialGenreListFromDB.get(i)).getName().equals("") ){isNameAlreadyUsed=true;
//                System.out.println("founf");break;}
//
//        }
//
//        if(isNameAlreadyUsed){InsertDB.insert(genreModel);}
//
//        String responseBody=GetAPI.get(Table.GENRE,nextIndexGenre);
//
//        responseBody="["+responseBody+"]";
//       System.out.println(responseBody);
//        ObjectMapper mapper = new ObjectMapper();
//
//        List<Object> genreListFromAPIResponse = mapper.readValue(responseBody,
//                new TypeReference<List<GenreModel>>() {});
//
//        List<Object> genreListFromDB= SelectDB.select(Table.GENRE,nextIndexGenre);
//
//        cleanModel(Table.GENRE,nextIndexGenre);
//
//        Assert.assertEquals(genreListFromAPIResponse.size(),genreListFromDB.size(),"check for matching  list size(should be 1) get from API and DB");
//        Assert.assertTrue(identicalListOfModel(genreListFromAPIResponse,genreListFromDB),"check for matching  genre  get from API and DB");
//
//    }




//    @Test
//    public static void testGetAllGenresWhereOneIsWithNameEmpty() throws IOException {

//        GenreModel genreModel = getRandomGenreModel(6);
//
//        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
//
//        genreModel.setName("");
//
//        InsertDB.insert(genreModel);
//
//        String responseBody=GetAPI.get(Table.GENRE);
////       System.out.println(responseBody);
//        ObjectMapper mapper = new ObjectMapper();
//
//        List<Object> genreListFromAPIResponse = mapper.readValue(responseBody,
//                new TypeReference<List<GenreModel>>() {});
//
//        List<Object> genreListFromDB= SelectDB.select(Table.GENRE);
//
////        cleanModel(Table.GENRE,nextIndexGenre);
//
//        Assert.assertEquals(genreListFromAPIResponse.size(),genreListFromDB.size(),"check for matching  list size(should be 1) get from API and DB");
//        Assert.assertTrue(identicalListOfModel(genreListFromAPIResponse,genreListFromDB),"check for matching  genre  get from API and DB");
//
//    }





}

