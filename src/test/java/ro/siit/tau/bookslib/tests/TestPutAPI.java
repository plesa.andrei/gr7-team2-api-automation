package ro.siit.tau.bookslib.tests;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;
import ro.siit.tau.bookslib.models.AuthorModel;
import ro.siit.tau.bookslib.models.BookModel;
import ro.siit.tau.bookslib.models.GenreModel;
import ro.siit.tau.bookslib.services.*;

import java.io.IOException;

import static ro.siit.tau.bookslib.utils.DBUtil.*;



public class TestPutAPI extends BaseTest{

    private static AuthorModel prepareAuthorForPut(){

        AuthorModel authorModel = getRandomAuthorModel(8);

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        authorModel.setId(nextIndexAuthor);

        InsertDB.insert(authorModel);

        return authorModel;

    }

    private static GenreModel prepareGenreForPut(){

        GenreModel genreModel = getRandomGenreModel(8);

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        genreModel.setId(nextIndexGenre);

        InsertDB.insert(genreModel);

        return genreModel;

    }

    private static BookModel prepareBookForPut(){

        AuthorModel authorModel = getRandomAuthorModel(7);
        GenreModel genreModel = getRandomGenreModel(5);
        BookModel bookModel = getRandomBookModel(6);


        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        InsertDB.insert(authorModel);
        InsertDB.insert(genreModel);

        authorModel.setId(nextIndexAuthor);
        genreModel.setId(nextIndexGenre);

        bookModel.setAuthor(authorModel);
        bookModel.setGenre(genreModel);
        bookModel.setId(nextIndexBook);

        InsertDB.insert(bookModel);

        return bookModel;

    }

    private static BookModel prepareBookForPutWithoutInsertInDB(){

        AuthorModel authorModelForPut = getRandomAuthorModel(8);
        GenreModel genreModelForPut = getRandomGenreModel(9);
        BookModel bookModelForPut = getRandomBookModel(10);


        int nextIndexAuthorForPut = getNextAutoIncrement(Table.AUTHOR);
        int nextIndexGenreForPut  = getNextAutoIncrement(Table.GENRE);
        int nextIndexBookForPut  = getNextAutoIncrement(Table.BOOK);

        InsertDB.insert(authorModelForPut );
        InsertDB.insert(genreModelForPut );

        authorModelForPut.setId(nextIndexAuthorForPut );
        genreModelForPut.setId(nextIndexGenreForPut );

        bookModelForPut.setAuthor(authorModelForPut );
        bookModelForPut.setGenre(genreModelForPut );
        bookModelForPut.setId(nextIndexBookForPut-1);

        return bookModelForPut;

    }






    @Test
    public static void testPutAuthor() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//always before preparation

        prepareAuthorForPut();

        AuthorModel authorModelForPUT = getRandomAuthorModel(9);

        authorModelForPUT.setId(nextIndexAuthor);

        String putMessageReceived = PutAPI.put(authorModelForPUT,nextIndexAuthor);

        AuthorModel authorModelGetFromDB = (AuthorModel)(SelectDB.select(Table.AUTHOR, nextIndexAuthor).get(0));

        cleanModel(Table.AUTHOR, nextIndexAuthor);

        Assert.assertTrue(putMessageReceived.contains("Success"), "check if  message received after PUT-ing Author from API is successful ");

        Assert.assertTrue(modelsHaveEqualValues(authorModelForPUT, authorModelGetFromDB), "check if the random data model PUT-ed through API is also  find in DB");

        System.out.println("message" + putMessageReceived);
//        printOneDataModels(authorModel);
//        printOneDataModels(authorModelGetFromDB);
//        printAllDataModels(SelectDB.select(Table.AUTHOR, nextIndexAuthor));


    }

    @Test
    public static void testPutAuthorAlreadyExists() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        AuthorModel authorModel=prepareAuthorForPut();

        String putMessageReceived = PutAPI.put(authorModel,nextIndexAuthor);
//        System.out.println("message" + putMessageReceived);
        cleanModel(Table.AUTHOR, nextIndexAuthor);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"Author already exists!\""), "check if  message received after PUT-ing Author from API is successful ");

    }

    @Test
    public static void testPutAuthorWithNoExistingID() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);

        AuthorModel authorModel=getRandomAuthorModel(8);

        String putMessageReceived = PutAPI.put(authorModel,nextIndexAuthor);
//        System.out.println("message" + putMessageReceived);
        Assert.assertTrue(putMessageReceived.contains("\"message\":\"Author with id "+nextIndexAuthor+" was not found\""), "check if  message received after PUT-ing Author from API is successful ");
//        cleanModel(Table.AUTHOR, nextIndexAuthor);
    }

    @Test
    public static void testPutAuthorWithNullFirstName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//always before preparation

        prepareAuthorForPut();

        AuthorModel authorModelForPUT = getRandomAuthorModel(9);

        authorModelForPUT.setId(nextIndexAuthor);
        authorModelForPUT.setFirstName(null);

        String putMessageReceived = PutAPI.put(authorModelForPUT,nextIndexAuthor);
//        System.out.println("message" + putMessageReceived);
        cleanModel(Table.AUTHOR, nextIndexAuthor);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"Missing first name\""), "check if  message received after PUT-ing Author from API is successful ");

    }

    @Test
    public static void testPutAuthorWithNullLastName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//always before preparation

        prepareAuthorForPut();

        AuthorModel authorModelForPUT = getRandomAuthorModel(9);

        authorModelForPUT.setId(nextIndexAuthor);
        authorModelForPUT.setLastName(null);

        String putMessageReceived = PutAPI.put(authorModelForPUT,nextIndexAuthor);
//        System.out.println("message" + putMessageReceived);
        cleanModel(Table.AUTHOR, nextIndexAuthor);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"Missing last name\""), "check if  message received after PUT-ing Author from API is successful ");

    }

    @Test
    public static void testPutAuthorWithEmptyFirstName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//always before preparation

        prepareAuthorForPut();

        AuthorModel authorModelForPUT = getRandomAuthorModel(9);

        authorModelForPUT.setId(nextIndexAuthor);
        authorModelForPUT.setFirstName("");

        String putMessageReceived = PutAPI.put(authorModelForPUT,nextIndexAuthor);
//      System.out.println("message" + putMessageReceived);
        cleanModel(Table.AUTHOR, nextIndexAuthor);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"The author first name should have at least 1 character\""), "check if  message received after PUT-ing Author from API is successful ");

    }

    @Test
    public static void testPutAuthorWithEmptyLastName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//always before preparation

        prepareAuthorForPut();

        AuthorModel authorModelForPUT = getRandomAuthorModel(9);

        authorModelForPUT.setId(nextIndexAuthor);
        authorModelForPUT.setLastName("");

        String putMessageReceived = PutAPI.put(authorModelForPUT,nextIndexAuthor);
//      System.out.println("message" + putMessageReceived);
        cleanModel(Table.AUTHOR, nextIndexAuthor);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"The author last name should have at least 1 character\""), "check if  message received after PUT-ing Author from API is successful ");

    }

    @Test
    public static void testPutAuthorWithLongerFirstName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//always before preparation

        prepareAuthorForPut();

        AuthorModel authorModelForPUT = getRandomAuthorModel(9);

        authorModelForPUT.setId(nextIndexAuthor);
        authorModelForPUT.setFirstName("more than 45 characters 123456789012345678901234567890123456789012345678901234567890");

        String putMessageReceived = PutAPI.put(authorModelForPUT,nextIndexAuthor);
//     System.out.println("message" + putMessageReceived);
        cleanModel(Table.AUTHOR, nextIndexAuthor);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"The author first name MAX length is 45\""), "check if  message received after PUT-ing Author from API is successful ");

    }

    @Test
    public static void testPutAuthorWithLongerLastName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//always before preparation

        prepareAuthorForPut();

        AuthorModel authorModelForPUT = getRandomAuthorModel(9);

        authorModelForPUT.setId(nextIndexAuthor);
        authorModelForPUT.setLastName("more than 45 characters 123456789012345678901234567890123456789012345678901234567890");

        String putMessageReceived = PutAPI.put(authorModelForPUT,nextIndexAuthor);
//     System.out.println("message" + putMessageReceived);
        cleanModel(Table.AUTHOR, nextIndexAuthor);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"The author last name MAX length is 45\""), "check if  message received after PUT-ing Author from API is successful ");

    }













    @Test
    public static void testPutBook() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning purposes
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        prepareBookForPut();

        int nextIndexAuthorForPut = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenreForPut  = getNextAutoIncrement(Table.GENRE);//for cleaning purposes
        //int nextIndexBookForPut  = getNextAutoIncrement(Table.BOOK);

        BookModel bookModelForPut=prepareBookForPutWithoutInsertInDB();

        String putMessageReceived = PutAPI.put(bookModelForPut,nextIndexBook);
//       System.out.println("message" + putMessageReceived);
        BookModel bookModelGetFromDB = (BookModel)(SelectDB.select(Table.BOOK, nextIndexBook).get(0));

        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);
        cleanModel(Table.GENRE,nextIndexGenreForPut);
        cleanModel(Table.AUTHOR,nextIndexAuthorForPut);

        Assert.assertTrue(putMessageReceived.contains("Success"), "check if  message received after PUT-ing Book from API is successful ");

        Assert.assertTrue(modelsHaveEqualValues(bookModelForPut, bookModelGetFromDB), "check if the random data model PUT-ed through API is also  find in DB");

    }

    @Test
    public static void testPutBookAlreadyExists() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning purposes
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        BookModel bookModelForPut =  prepareBookForPut();

        String putMessageReceived = PutAPI.put(bookModelForPut,nextIndexBook);
//       System.out.println("message" + putMessageReceived);
        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);

        throw new SkipException("Skipping for discussion");


//        Assert.assertFalse(putMessageReceived.contains("Success"), "check if  message received after PUT-ing Book TWICE from API is NOT successful ");

    }

    @Test
    public static void testPutBookWithNonExistingID() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning purposes
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);//for cleaning purposes

        BookModel bookModelForPut=getRandomBookModel(8);

        String putMessageReceived = PutAPI.put(bookModelForPut,nextIndexBook);
//      System.out.println("message" + putMessageReceived);
        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);//just in case

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"Book with id "+nextIndexBook+" was not found\""), "check if  message received after PUT-ing Book from API is one expected ");

    }

    @Test
    public static void testPutBookWithNullName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning purposes
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        prepareBookForPut();

        int nextIndexAuthorForPut = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenreForPut  = getNextAutoIncrement(Table.GENRE);//for cleaning purposes

        BookModel bookModelForPut=prepareBookForPutWithoutInsertInDB();

        bookModelForPut.setName(null);

        String putMessageReceived = PutAPI.put(bookModelForPut,nextIndexBook);
//       System.out.println("message" + putMessageReceived);
        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);
        cleanModel(Table.GENRE,nextIndexGenreForPut);
        cleanModel(Table.AUTHOR,nextIndexAuthorForPut);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"Missing book name\""), "check if  message received after PUT-ing Book from API is the one expected ");

    }

    @Test
    public static void testPutBookWithNullAuthor() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning purposes
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        prepareBookForPut();

        int nextIndexAuthorForPut = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenreForPut  = getNextAutoIncrement(Table.GENRE);//for cleaning purposes

        BookModel bookModelForPut=prepareBookForPutWithoutInsertInDB();

        bookModelForPut.setAuthor(null);

        String putMessageReceived = PutAPI.put(bookModelForPut,nextIndexBook);
//       System.out.println("message" + putMessageReceived);
        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);
        cleanModel(Table.GENRE,nextIndexGenreForPut);
        cleanModel(Table.AUTHOR,nextIndexAuthorForPut);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"Missing author\""), "check if  message received after PUT-ing Book from API is the one expected ");

    }

    @Test
    public static void testPutBookWithNullGenre() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning purposes
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        prepareBookForPut();

        int nextIndexAuthorForPut = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenreForPut  = getNextAutoIncrement(Table.GENRE);//for cleaning purposes

        BookModel bookModelForPut=prepareBookForPutWithoutInsertInDB();

        bookModelForPut.setGenre(null);

        String putMessageReceived = PutAPI.put(bookModelForPut,nextIndexBook);
//       System.out.println("message" + putMessageReceived);
        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);
        cleanModel(Table.GENRE,nextIndexGenreForPut);
        cleanModel(Table.AUTHOR,nextIndexAuthorForPut);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"Missing genre\""), "check if  message received after PUT-ing Book from API is the one expected ");

    }

    @Test
    public static void testPutBookWithNullPublicationDate() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning purposes
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        prepareBookForPut();

        int nextIndexAuthorForPut = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenreForPut  = getNextAutoIncrement(Table.GENRE);//for cleaning purposes

        BookModel bookModelForPut=prepareBookForPutWithoutInsertInDB();

        bookModelForPut.setPublicationDate(null);

        String putMessageReceived = PutAPI.put(bookModelForPut,nextIndexBook);
//       System.out.println("message" + putMessageReceived);
        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);
        cleanModel(Table.GENRE,nextIndexGenreForPut);
        cleanModel(Table.AUTHOR,nextIndexAuthorForPut);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"Missing publication date\""), "check if  message received after PUT-ing Book from API is the one expected ");

    }

    @Test
    public static void testPutBookWithEmptyName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning purposes
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        prepareBookForPut();

        int nextIndexAuthorForPut = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenreForPut  = getNextAutoIncrement(Table.GENRE);//for cleaning purposes

        BookModel bookModelForPut=prepareBookForPutWithoutInsertInDB();

        bookModelForPut.setName("");

        String putMessageReceived = PutAPI.put(bookModelForPut,nextIndexBook);
//       System.out.println("message" + putMessageReceived);
        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);
        cleanModel(Table.GENRE,nextIndexGenreForPut);
        cleanModel(Table.AUTHOR,nextIndexAuthorForPut);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"The book name should have at least 1 character\""), "check if  message received after PUT-ing Book from API is the one expected ");

    }

    @Test
    public static void testPutBookWithEmptyPublicationDate() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning purposes
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        prepareBookForPut();

        int nextIndexAuthorForPut = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenreForPut  = getNextAutoIncrement(Table.GENRE);//for cleaning purposes

        BookModel bookModelForPut=prepareBookForPutWithoutInsertInDB();

        bookModelForPut.setPublicationDate("");

        String putMessageReceived = PutAPI.put(bookModelForPut,nextIndexBook);
//       System.out.println("message" + putMessageReceived);
        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);
        cleanModel(Table.GENRE,nextIndexGenreForPut);
        cleanModel(Table.AUTHOR,nextIndexAuthorForPut);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"Missing publication date\""), "check if  message received after PUT-ing Book from API is the one expected ");

    }

    @Test
    public static void testPutBookWithLongerName() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning purposes
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        prepareBookForPut();

        int nextIndexAuthorForPut = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenreForPut  = getNextAutoIncrement(Table.GENRE);//for cleaning purposes

        BookModel bookModelForPut=prepareBookForPutWithoutInsertInDB();

        bookModelForPut.setName("more than 45 characters 12345678901234567890123456789012345678901234567890");

        String putMessageReceived = PutAPI.put(bookModelForPut,nextIndexBook);
//       System.out.println("message" + putMessageReceived);
        cleanModel(Table.BOOK,nextIndexBook,Table.AUTHOR,nextIndexAuthor,Table.GENRE,nextIndexGenre);
        cleanModel(Table.GENRE,nextIndexGenreForPut);
        cleanModel(Table.AUTHOR,nextIndexAuthorForPut);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"The book name MAX length is 45\""), "check if  message received after PUT-ing Book from API is the one expected ");

    }

    @Test
    public static void testPutBookWithInvalidMonthPublicationDate() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning purposes
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        prepareBookForPut();

        int nextIndexAuthorForPut = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenreForPut  = getNextAutoIncrement(Table.GENRE);//for cleaning purposes

        BookModel bookModelForPut=prepareBookForPutWithoutInsertInDB();

        int month=getRandomNumber(55,88);
        String publicationDateString=getRandomNumber(1970,2019)+"-"+month+"-"+getRandomNumber(1,27);

        bookModelForPut.setPublicationDate(publicationDateString);

        String putMessageReceived = PutAPI.put(bookModelForPut,nextIndexBook);
//        System.out.println("message" + putMessageReceived);
        BookModel bookModelGetFromDB = (BookModel)(SelectDB.select(Table.BOOK, nextIndexBook).get(0));

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
        cleanModel(Table.GENRE, nextIndexGenreForPut);
        cleanModel(Table.AUTHOR, nextIndexAuthorForPut);

        throw new SkipException("Skipping for discussion");


//        Assert.assertFalse(putMessageReceived.contains("\"message\":\"Success!\""), "  PUT-ing by mistake Book from API with wrong(invalid month='" + month + "')  publication date put-ed='" + publicationDateString + "' and the one found after in DB=" + bookModelGetFromDB.getPublicationDate());
//        Assert.assertTrue(modelsHaveEqualValues(bookModelForPut, bookModelGetFromDB), "  check if   wrong(invalid month='" + month + "') of publication date put-ed='" + publicationDateString + "' and the one found after in DB=" + bookModelGetFromDB.getPublicationDate() + " have matching values");

    }

    @Test
    public static void testPutBookWithInvalidDayPublicationDate() throws IOException {

        int nextIndexAuthor = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//for cleaning purposes
        int nextIndexBook = getNextAutoIncrement(Table.BOOK);

        prepareBookForPut();

        int nextIndexAuthorForPut = getNextAutoIncrement(Table.AUTHOR);//for cleaning purposes
        int nextIndexGenreForPut  = getNextAutoIncrement(Table.GENRE);//for cleaning purposes

        BookModel bookModelForPut=prepareBookForPutWithoutInsertInDB();

        int day=getRandomNumber(55,88);
        String publicationDateString=getRandomNumber(1970,2019)+"-"+getRandomNumber(1,12)+"-"+day;

        bookModelForPut.setPublicationDate(publicationDateString);

        String putMessageReceived = PutAPI.put(bookModelForPut,nextIndexBook);
//       System.out.println("message" + putMessageReceived);
        BookModel bookModelGetFromDB = (BookModel)(SelectDB.select(Table.BOOK, nextIndexBook).get(0));

        cleanModel(Table.BOOK, nextIndexBook, Table.AUTHOR, nextIndexAuthor, Table.GENRE, nextIndexGenre);
        cleanModel(Table.GENRE, nextIndexGenreForPut);
        cleanModel(Table.AUTHOR, nextIndexAuthorForPut);

        throw new SkipException("Skipping for discussion");


//        Assert.assertFalse(putMessageReceived.contains("\"message\":\"Success!\""), "  PUT-ing by mistake Book from API with wrong(invalid day='" + day + "')  publication date put-ed='" + publicationDateString + "' and the one found after in DB=" + bookModelGetFromDB.getPublicationDate());
//        Assert.assertTrue(modelsHaveEqualValues(bookModelForPut, bookModelGetFromDB), "  check if   wrong(invalid day='" + day + "') of publication date put-ed='" + publicationDateString + "' and the one found after in DB=" + bookModelGetFromDB.getPublicationDate() + " have matching values");

    }





















    @Test
    public static void testPutGenre() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        prepareGenreForPut();

        GenreModel genreModelForPUT = getRandomGenreModel(12);

        genreModelForPUT.setId(nextIndexGenre);

        String putMessageReceived = PutAPI.put(genreModelForPUT,nextIndexGenre);

        GenreModel genreModelGetFromDB = (GenreModel)(SelectDB.select(Table.GENRE, nextIndexGenre).get(0));

        cleanModel(Table.GENRE,nextIndexGenre);

        Assert.assertTrue(putMessageReceived.contains("Success"), "check if  message received after PUT-ing genre from API is successful ");

        Assert.assertTrue(modelsHaveEqualValues(genreModelForPUT, genreModelGetFromDB), "check if the random data model PUT-ed through API is also  find in DB");

    }

    @Test
    public static void testPutGenreAlreadyExists() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        GenreModel genreModel=prepareGenreForPut();

        String putMessageReceived = PutAPI.put(genreModel,nextIndexGenre);
//        System.out.println("message" + putMessageReceived);
        cleanModel(Table.GENRE, nextIndexGenre);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"Genre already exists\""), "check if  message received after PUT-ing Genre from API is the one expected ");

    }

    @Test
    public static void testPutGenreWithNoExistingID() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);

        GenreModel genreModel=getRandomGenreModel(8);

        String putMessageReceived = PutAPI.put(genreModel,nextIndexGenre);
        System.out.println("message" + putMessageReceived);
        Assert.assertTrue(putMessageReceived.contains("\"message\":\"Genre was not found\""), "check if  message received after PUT-ing Genre from API is the one expected ");
//        cleanModel(Table.AUTHOR, nextIndexAuthor);
    }

    @Test
    public static void testPutGenreWithNullName() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//always before preparation

        prepareGenreForPut();

        GenreModel genreModelForPUT = getRandomGenreModel(9);

        genreModelForPUT.setId(nextIndexGenre);
        genreModelForPUT.setName(null);

        String putMessageReceived = PutAPI.put(genreModelForPUT,nextIndexGenre);
//        System.out.println("message" + putMessageReceived);
        cleanModel(Table.GENRE, nextIndexGenre);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"Missing genre name\""), "check if  message received after PUT-ing Genre from API is the one expected ");

    }

    @Test
    public static void testPutGenreWithEmptyName() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//always before preparation

        prepareGenreForPut();

        GenreModel genreModelForPUT = getRandomGenreModel(9);

        genreModelForPUT.setId(nextIndexGenre);
        genreModelForPUT.setName("");

        String putMessageReceived = PutAPI.put(genreModelForPUT,nextIndexGenre);
//        System.out.println("message" + putMessageReceived);
        cleanModel(Table.GENRE, nextIndexGenre);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"The genre name should have at least 1 character\""), "check if  message received after PUT-ing Genre from API is the one expected ");

    }

    @Test
    public static void testPutGenreWithLongerName() throws IOException {

        int nextIndexGenre = getNextAutoIncrement(Table.GENRE);//always before preparation

        prepareGenreForPut();

        GenreModel genreModelForPUT = getRandomGenreModel(9);

        genreModelForPUT.setId(nextIndexGenre);
        genreModelForPUT.setName("more than 45 characters 12345678901234567890123456789012345678901234567890");

        String putMessageReceived = PutAPI.put(genreModelForPUT,nextIndexGenre);
        System.out.println("message" + putMessageReceived);
        cleanModel(Table.GENRE, nextIndexGenre);

        Assert.assertTrue(putMessageReceived.contains("\"message\":\"The genre name MAX length is 45\""), "check if  message received after PUT-ing Genre from API is the one expected ");

    }



}
